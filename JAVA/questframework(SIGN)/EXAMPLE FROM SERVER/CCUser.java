package com.abstractwolf.quests.cc;

import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-10-22.
 */
public class CCUser
{

    public CCUser(Player player)
    {
        this.player = player;
    }


    private Player player;
    private String firstName, lastName;

    public void setPlayer(Player player)
    {
        this.player = player;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }
}
