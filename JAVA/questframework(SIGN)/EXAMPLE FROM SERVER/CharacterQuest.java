package com.abstractwolf.quests.cc;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import com.abstractwolf.quests.AbstractAddons;
import com.abstractwolf.quests.Quest;
import com.abstractwolf.quests.part.QuestPart;
import com.abstractwolf.quests.utils.UtilChat;
import com.abstractwolf.quests.utils.json.JSONMessage;
import com.abstractwolf.quests.json.sub.JSONClickType;
import com.abstractwolf.quests.utils.json.sub.JSONColour;
import com.abstractwolf.quests.utils.json.sub.JSONExtra;
import com.abstractwolf.quests.utils.json.sub.JSONHoverType;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public class CharacterQuest extends Quest implements Listener
{

    public CharacterQuest(final AbstractAddons addons)
    {
        super("character_quest", "Character Creation", new String[]
                {
                        "Welcome to the Character Creation Quest!",
                        "This is the first Quest on Lumos-MC.",
                        "Click the sign in-front of you to continue.",
                        "Be sure to check the description of each part..",
                });

        this.addons = addons;

        addQuestPart(new QuestPart("firstname", 1, this, "Choose your first name!", new String[]
                {
                        "Hey dude! So you want to choose your first name? Awesome!",
                        "Type in chat the name you want to assign it. Please include colour codes!",
                        "After you reach the last quest part you will receive the name."
                }, new Location(Bukkit.getWorld("Hogwarts") , -133, 44, 218))
                {
                     @Override
                     public void onClick(Player player)
                     {
                         player.teleport(new Location(Bukkit.getWorld("Hogwarts"), -166, 44, 247, 1, -1)); // 2

                         JSONMessage link = new JSONMessage("To find all the colour codes go to ", JSONColour.AQUA);
                         JSONExtra linkExtra = new JSONExtra("HERE", JSONColour.DARK_AQUA);
                         linkExtra.setClickEvent(JSONClickType.OPEN_URL, "http://wiki.ess3.net/mc/");
                         linkExtra.setHoverEvent(JSONHoverType.SHOW_TEXT, "&aClick to view all colour codes!");
                         link.addExtra(linkExtra);
                         link.sendToPlayer(player);
                     }
                });

        addQuestPart(new QuestPart("lastname", 2, this, "Choose your last name!", new String[]
                {
                        "This will assign you a last name for all your roleplay needs!",
                        "Type in chat the name you want to assign it. Please include colour codes!",
                        "After you reach the last quest part you will receive the name."
                }, new Location(Bukkit.getWorld("Hogwarts") , -166, 44, 254))
                {

                    @Override
                    public void onClick(Player player)
                    {
                        player.teleport(new Location(Bukkit.getWorld("Hogwarts"), -165, 44, 329, 1, 1)); //3
                    }
                });

        addQuestPart(new QuestPart("bloodtype", 3, this, "Choose your blood type", new String[]
                {
                        "This feature is not currently implemented!",
                        "We will allow all users to retake this quest",
                        "once we finish it! Click the sign to continue."
                }, new Location(Bukkit.getWorld("Hogwarts") , -166, 44, 336))
        {

            @Override
            public void onClick(Player player)
            {
                player.teleport(new Location(Bukkit.getWorld("Hogwarts"), -166, 44, 211, -179, 5)); //4
            }
        });

        addQuestPart(new QuestPart("creator_finish", 4, this, "Finish", new String[]
                {
                        "Great! You've finished the Character Creator!",
                        "You've been teleported to the Hogwarts Express.",
                }, new Location(Bukkit.getWorld("Hogwarts") , -166, 44, 201))
        {

            @Override
            public void onClick(Player player)
            {
                CCUser ccUser = addons.getCcManager().getCCUser(player);
                updateNick(player, ccUser.getFirstName() + ccUser.getLastName());
                addons.getCcManager().removeCCUser(player);
                player.teleport(new Location(Bukkit.getWorld("Hogwarts"), -66, 72, -424, -89, 5)); //Train
            }
        });
    }

    private AbstractAddons addons;

    @Override
    public void onStart(Player player)
    {
        player.teleport(new Location(Bukkit.getWorld("Hogwarts"), -133, 44, 211, 0, 5)); //1
        addons.getCcManager().setCCUser(player);

        JSONMessage link = new JSONMessage("To find all the colour codes go to ", JSONColour.AQUA);
        JSONExtra linkExtra = new JSONExtra("HERE", JSONColour.DARK_AQUA);
        linkExtra.setClickEvent(JSONClickType.OPEN_URL, "http://wiki.ess3.net/mc/");
        linkExtra.setHoverEvent(JSONHoverType.SHOW_TEXT, "&aClick to view all colour codes!");
        link.addExtra(linkExtra);
        link.sendToPlayer(player);
    }

    @EventHandler
    public void onTalk(AsyncPlayerChatEvent event)
    {

        if (addons.getQuestManager().getPlayersActiveQuest(event.getPlayer()) != null && addons.getQuestManager().getPlayersActiveQuest(event.getPlayer()).getTitle().equalsIgnoreCase(getTitle()))
        {

            int id = (addons.getQuestManager().getQuestPlayer(event.getPlayer()).getActiveQuestPart() == null ? 0 : addons.getQuestManager().getQuestPlayer(event.getPlayer()).getActiveQuestPart().getNumberID());
            event.setCancelled(true);

            switch (id)
            {
                case 1:
                    if (event.getMessage().length() < 1 || event.getMessage().length() > 32)
                    {
                        UtilChat.message(event.getPlayer(), "Error", "Your nickname cannot be longer than 32 characters or contain an &");
                        return;
                    }

                    addons.getCcManager().getCCUser(event.getPlayer()).setFirstName(event.getMessage());
                    UtilChat.message(event.getPlayer(), "Character Creation", "Your first name was updated to: " + event.getMessage());
                    break;
                case 2:
                    if (event.getMessage().length() < 1 || event.getMessage().length() > 32)
                    {
                        UtilChat.message(event.getPlayer(), "Error", "Your nickname cannot be longer than 32 characters");
                        return;
                    }

                    addons.getCcManager().getCCUser(event.getPlayer()).setLastName(event.getMessage());
                    UtilChat.message(event.getPlayer(), "Character Creation", "Your last name was updated to: " + event.getMessage());
                    break;
                case 5:
                    UtilChat.message(event.getPlayer(), "Error", "This feature is not released yet! Check back soon.");
                    break;
                default:
                    event.setCancelled(false);
                    break;
            }
        }
    }

    public void updateNick(Player player, String nickname)
    {
        User user = ((Essentials) Bukkit.getPluginManager().getPlugin("Essentials")).getUser(player);
        setNickname(user, nickname);
    }

    private void setNickname(final User target, final String nickname)
    {
        target.setNickname(ChatColor.translateAlternateColorCodes('&', nickname));
        target.setDisplayNick();
    }
}
