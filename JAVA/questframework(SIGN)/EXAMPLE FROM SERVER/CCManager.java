package com.abstractwolf.quests.cc;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public class CCManager
{

    private Map<UUID, CCUser> userInformation = new HashMap<>();

    public void setCCUser(Player player) { userInformation.put(player.getUniqueId(), new CCUser(player));}
    public void removeCCUser(Player player) { userInformation.remove(player.getUniqueId()); }

    public CCUser getCCUser(Player player)
    {
        if (userInformation.containsKey(player.getUniqueId()))
            return userInformation.get(player.getUniqueId());

        return null;
    }

    public Map<UUID, CCUser> getUserInformation()
    {
        return userInformation;
    }
}
