package com.abstractwolf.quest.listener;

import com.abstractwolf.LumosAddons;
import com.abstractwolf.quest.Quest;
import com.abstractwolf.quest.part.QuestPart;
import mcom.abstractwolf.utils.UtilChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public class QuestSignListeners implements Listener
{

    public QuestSignListeners(LumosAddons addons)
    {
        this.addons = addons;
    }

    private LumosAddons addons;

        /*
          [QuestID]
          [Quest Name]
          [Part ID]
         */

    @EventHandler
    public void onCreateQuest(SignChangeEvent event)
    {

        Player player = event.getPlayer();

        for (Quest quests : addons.getQuestManager().getQuestList())
        {
            if (event.getLine(0).equalsIgnoreCase(quests.getId()) && event.getLine(1).equalsIgnoreCase(quests.getTitle()))
            {
                UtilChat.message(player, "Sign Generator", "Creating a new Quest sign! Quest ID: " + quests.getId() + " Quest Name: " + quests.getTitle());
                event.setLine(0, ChatColor.DARK_AQUA + quests.getId());
                event.setLine(1, ChatColor.AQUA + quests.getTitle());
                event.setLine(2, ChatColor.AQUA + "Click to Start!");

                UtilChat.message(player, "Sign Generator", "Quest sign generated, now to generate the part signs around the map. :)");

                for (QuestPart questParts : quests.getQuestPart())
                {

                    if (questParts.getSignLocation().getBlock() == null || questParts.getSignLocation().getBlock().getType().equals(Material.AIR))
                    {
                        System.out.println("A Sign at: " + questParts.getSignLocation().getBlock().getX() + ", " + questParts.getSignLocation().getBlock().getY() + ", " + questParts.getSignLocation().getBlock().getZ() + " did not exist!");
                        return;
                    }

                    if (questParts.getSignLocation().getBlock().getType() == Material.SIGN || questParts.getSignLocation().getBlock().getType() == Material.WALL_SIGN || questParts.getSignLocation().getBlock().getType() == Material.SIGN_POST)
                    {
                        Sign blockSign = (Sign) questParts.getSignLocation().getBlock().getState();
                        blockSign.setLine(0, ChatColor.DARK_AQUA.toString() + quests.getId());
                        blockSign.setLine(1, ChatColor.AQUA.toString() + quests.getTitle());
                        blockSign.setLine(2, ChatColor.AQUA.toString() + questParts.getNumberID());
                        blockSign.setLine(3, ChatColor.AQUA.toString() + questParts.getId());
                        blockSign.update(true, true);
                        UtilChat.message(player, "Sign Generator", "Part sign generated for Quest: " + blockSign.getLine(0) + ", ID: " + blockSign.getLine(2) + "");
                    }
                }
                continue;
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if (event.getClickedBlock().getType() == Material.SIGN || event.getClickedBlock().getType() == Material.SIGN_POST || event.getClickedBlock().getType() == Material.WALL_SIGN)
            {
                Player player = event.getPlayer();
                Sign sign = (Sign) event.getClickedBlock().getState();

                for (Quest quests : addons.getQuestManager().getQuestList())
                {
                    if (sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_AQUA + quests.getId()) && sign.getLine(1).equalsIgnoreCase(ChatColor.AQUA + quests.getTitle()) && sign.getLine(2).equalsIgnoreCase(ChatColor.AQUA + "Click to Start!"))
                    {
                        if (addons.getQuestManager().getPlayersActiveQuest(player) != null)
                        {
                            UtilChat.createLine(player);
                            UtilChat.message(player, "Quest", ChatColor.DARK_AQUA + "You are already in a Quest! Current Quest: " + ChatColor.AQUA + addons.getQuestManager().getPlayersActiveQuest(player).getTitle());
                            UtilChat.createLine(player);
                            return;
                        }

                        addons.getQuestManager().setActiveQuest(player, quests);
                        UtilChat.createLine(player);
                        UtilChat.message(player, "Quest", "Your active Quest has been set to: " + quests.getId());
                        UtilChat.message(player, "Information", ChatColor.GRAY + "This Quest has " + ChatColor.DARK_AQUA + quests.getQuestPart().size() + ChatColor.AQUA + " part(s)" + ChatColor.GRAY + " to complete!");
                        for (String desc : addons.getQuestManager().getPlayersActiveQuest(player).getDescription())
                            UtilChat.message(player, "Description", desc);
                        UtilChat.createLine(player);
                        addons.getQuestManager().getPlayersActiveQuest(player).onStart(player);
                        continue;
                    }
                    else if (addons.getQuestManager().getPlayersActiveQuest(player) != null && (sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_AQUA + quests.getId()) && sign.getLine(1).equalsIgnoreCase(ChatColor.AQUA + quests.getTitle()) && (addons.getQuestManager().getQuestPlayer(player).getActiveQuestPart() == null ? sign.getLine(2).equals(ChatColor.AQUA.toString() + "1") : sign.getLine(2).equals(ChatColor.AQUA.toString() + String.valueOf((addons.getQuestManager().getQuestPlayer(player).getActiveQuestPart().getNumberID() + 1))))))
                    {
                        for (QuestPart part : quests.getQuestPart())
                        {
                            if (sign.getLine(3).equalsIgnoreCase(ChatColor.AQUA + part.getId()))
                            {
                                try
                                {
                                    for (int i = 0; i < 25; i++)
                                        UtilChat.message(player, "");

                                    addons.getQuestManager().getQuestPlayer(player).setActiveQuestPart(part);
                                    addons.getQuestManager().getQuestPlayer(player).getActiveQuestPart().onClick(player);

                                    if (quests.getQuestPart().get((addons.getQuestManager().getQuestPlayer(player).getActiveQuestPart().getNumberID() - 1) + 1) == null)
                                        return;

                                    UtilChat.createLine(player);
                                    UtilChat.message(player, "Quest", "You've started a new part of  the quest:");
                                    UtilChat.message(player, "Title",  addons.getQuestManager().getQuestPlayer(player).getActiveQuestPart().getTitle());
                                    for (String desc : addons.getQuestManager().getQuestPlayer(player).getActiveQuestPart().getDescription())
                                        UtilChat.message(player, "Description", desc);
                                    UtilChat.createLine(player);

                                }
                                catch (Exception e)
                                {
                                    for (int i = 0; i < 25; i++)
                                        UtilChat.message(player, "");

                                    UtilChat.createLine(player);
                                    player.sendMessage(ChatColor.DARK_RED + "Congratulations! You completed " + ChatColor.RED + addons.getQuestManager().getQuestPlayer(player).getActiveQuest().getTitle() + ChatColor.DARK_RED + "!");
                                    UtilChat.createLine(player);
                                    addons.getQuestManager().removeActiveQuest(player);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
