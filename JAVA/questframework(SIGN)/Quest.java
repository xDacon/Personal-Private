package com.abstractwolf.quest;

import com.abstractwolf.quest.part.QuestPart;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public abstract class Quest
{

    public Quest(String id, String title, String[] description)
    {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    private String id, title;
    private String[] description;
    private List<QuestPart> questPart = new ArrayList<>();

    public void addQuestPart(QuestPart part)
    {
        if (questPart.contains(part))
            return;

        questPart.add(part);
        System.out.println("Added a new Quest Part to " + id + " Quest Part: " + part.getId());
    }

    public void onStart(Player player) {}

    public String getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public String[] getDescription()
    {
        return description;
    }

    public List<QuestPart> getQuestPart()
    {
        return questPart;
    }
}
