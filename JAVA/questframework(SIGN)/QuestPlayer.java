package com.abstractwolf.quest;

import com.abstractwolf.quest.part.QuestPart;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public class QuestPlayer
{

    public QuestPlayer(UUID uuid, Quest activeQuest)
    {
        this.uuid = uuid;
        this.activeQuest = activeQuest;
    }

    private UUID uuid;
    private Quest activeQuest;
    private QuestPart activeQuestPart;

    public UUID getUuid()
    {
        return uuid;
    }

    public Player getPlayerFromUUID()
    {
        return Bukkit.getPlayer(uuid);
    }

    public void setActiveQuest(Quest activeQuest)
    {
        this.activeQuest = activeQuest;
    }

    public void setActiveQuestPart(QuestPart activeQuestPart)
    {
        if (activeQuestPart.getOriginalQuest().equals(activeQuest))
            this.activeQuestPart = activeQuestPart;
        else
            System.out.println("That Part does not correspond with your Active Quest " + getPlayerFromUUID().getName());

    }

    public Quest getActiveQuest()
    {
        return activeQuest;
    }

    public QuestPart getActiveQuestPart()
    {
        return activeQuestPart;
    }
}
