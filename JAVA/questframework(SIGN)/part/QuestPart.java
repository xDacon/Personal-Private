package com.abstractwolf.quest.part;

import com.abstractwolf.quest.Quest;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public class QuestPart
{

    public QuestPart(String id, int numberID, Quest originalQuest, String title, String[] description, Location signLocation)
    {
        this.id = id;
        this.numberID = numberID;
        this.description = description;
        this.originalQuest = originalQuest;
        this.title = title;
        this.signLocation = signLocation;
    }

    private int numberID;
    private String id, title;
    private String[] description;
    private Quest originalQuest;
    private Location signLocation;

    public void onClick(Player player) {}

    public String getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public String[] getDescription()
    {
        return description;
    }

    public Location getSignLocation()
    {
        return signLocation;
    }

    public Quest getOriginalQuest()
    {
        return originalQuest;
    }

    public int getNumberID()
    {
        return numberID;
    }
}
