package com.abstractwolf.quest;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-10-23.
 */
public class QuestManager
{

    private HashSet<Quest> questList = new HashSet<>();
    private Map<UUID, QuestPlayer> activeQuest = new HashMap<>();

    public void addNewQuest(Quest quest) { questList.add(quest); }

    public void setActiveQuest(Player player, Quest quest)
    {
        activeQuest.put(player.getUniqueId(), new QuestPlayer(player.getUniqueId(), quest));
        System.out.println("Assigned " + quest.getId() + " to " + player.getName());
    }

    public void removeActiveQuest(Player player)
    {
        activeQuest.remove(player.getUniqueId());
        System.out.println("Removed all quests from: " + player.getName());
    }

    public Quest getPlayersActiveQuest(Player player)
    {
        if (activeQuest.containsKey(player.getUniqueId()))
            return activeQuest.get(player.getUniqueId()).getActiveQuest();

        return null;
    }

    public QuestPlayer getQuestPlayer(Player player)
    {
        if (activeQuest.containsKey(player.getUniqueId()))
            return activeQuest.get(player.getUniqueId());
        return null;
    }

    public HashSet<Quest> getQuestList()
    {
        return questList;
    }
}
