package com.abstractwolf.emoji.emojicommand;

import com.abstractwolf.emoji.AnimatedEmoji;
import com.abstractwolf.emoji.utils.ChatUtil;
import com.abstractwolf.emoji.utils.SkullBuilder;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class EmojiSelectCommand implements CommandExecutor
{

    public EmojiSelectCommand(AnimatedEmoji animatedEmoji)
    {
        this.animatedEmoji = animatedEmoji;
    }

    private AnimatedEmoji animatedEmoji;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
    {

        if (commandSender instanceof Player)
        {
            Player player = (Player) commandSender;

            if (args.length == 1)
            {
                if (player.hasPermission(animatedEmoji.getConfig().getString("Permissions.PermissionNode").replace("{emoji}", args[0])))
                {
                    if (animatedEmoji.getEmojiManager().getEmojiByName(args[0]) != null)
                    {
                        animatedEmoji.getEmojiManager().setCurrentEmoji(player, animatedEmoji.getEmojiManager().getEmojiByName(args[0]).getEmojiName());
                        player.getInventory().addItem(new SkullBuilder().setName(ChatColor.YELLOW + args[0]).setCustomSkull(animatedEmoji.getEmojiManager().getSelectedEmoji(player).getSkullId()).build());
                        ChatUtil.sendConfigMessage(player, "Chat.Messages.Selected", args[0]);
                    }
                    else
                    {
                        ChatUtil.sendConfigMessage(player, "Chat.Messages.NoEmoji", args[0]);
                    }
                }
                else
                {
                    ChatUtil.sendConfigMessage(player, "Chat.Messages.NoEmojiPermission", args[0]);
                }
            }
        }

        return false;
    }
}
