package com.abstractwolf.emoji.utils;

import com.abstractwolf.emoji.AnimatedEmoji;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class ChatUtil
{

    public ChatUtil(AnimatedEmoji animatedEmoji)
    {
        this.animatedEmoji = animatedEmoji;
    }

    private static AnimatedEmoji animatedEmoji;

    public static void sendMessage(Player player, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + message));
    }

    public static void sendMessage(Player player, Object prefix, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &8>> &7" + message));
    }

    public static void sendConfigMessage(Player player, String configPath, String emoji)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', animatedEmoji.getConfig().getString("Chat.Prefix") + " &8>> &7" + animatedEmoji.getConfig().getString(configPath).replace("{emoji}", emoji)));
    }
}
