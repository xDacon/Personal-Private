package com.abstractwolf.emoji;

import com.abstractwolf.emoji.api.manager.EmojiManager;
import com.abstractwolf.emoji.emojicommand.EmojiSelectCommand;
import com.abstractwolf.emoji.listener.EmojiListener;
import com.abstractwolf.emoji.types.WinkEmoji;
import com.abstractwolf.emoji.utils.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class AnimatedEmoji extends JavaPlugin
{

    private EmojiManager emojiManager;

    @Override
    public void onEnable()
    {

        new ChatUtil(this);

        emojiManager = new EmojiManager();
        emojiManager.addEmoji(new WinkEmoji(this));

        getCommand("emoji").setExecutor(new EmojiSelectCommand(this));
        Bukkit.getPluginManager().registerEvents(new EmojiListener(this), this);

        saveConfig();
    }

    @Override
    public void onDisable()
    {
        emojiManager.getCurrentEmoji().clear();
        emojiManager.getEmojiHashSet().clear();
    }

    public EmojiManager getEmojiManager()
    {
        return emojiManager;
    }
}
