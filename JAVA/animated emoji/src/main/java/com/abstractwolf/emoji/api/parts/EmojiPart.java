package com.abstractwolf.emoji.api.parts;

import com.abstractwolf.emoji.api.Emoji;
import com.abstractwolf.emoji.utils.SkullBuilder;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class EmojiPart
{

    public EmojiPart(Emoji originalEmoji, String emojiSkullID)
    {
        this.originalEmoji = originalEmoji;
        this.emojiSkullID = emojiSkullID;
        itemStack = new SkullBuilder().setName(ChatColor.YELLOW + originalEmoji.getEmojiName()).setCustomSkull(emojiSkullID).build();
    }

    private Emoji originalEmoji;
    private String emojiSkullID;
    private ItemStack itemStack;

    /**
     * Puts the skull on a players head.
     * @param player
     */
    public void build(Player player)
    {
        player.getInventory().setHelmet(itemStack);
    }

    public Emoji getOriginalEmoji()
    {
        return originalEmoji;
    }

    public String getEmojiSkullID()
    {
        return emojiSkullID;
    }
}
