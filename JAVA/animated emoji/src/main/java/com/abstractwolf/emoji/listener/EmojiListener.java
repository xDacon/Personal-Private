package com.abstractwolf.emoji.listener;

import com.abstractwolf.emoji.AnimatedEmoji;
import com.abstractwolf.emoji.utils.ChatUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class EmojiListener implements Listener
{

    public EmojiListener(AnimatedEmoji animatedEmoji)
    {
        this.animatedEmoji = animatedEmoji;
    }

    private AnimatedEmoji animatedEmoji;

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        if (animatedEmoji.getEmojiManager().playerHasEmojiSelected(event.getPlayer()))
            animatedEmoji.getEmojiManager().getCurrentEmoji().remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {

        if (animatedEmoji.getEmojiManager().playerHasEmojiSelected(event.getPlayer()))
        {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)
            {
                String clickedItemDisplay = event.getItem().getItemMeta().getDisplayName();
                String requiredDisplay = animatedEmoji.getEmojiManager().getSelectedEmoji(event.getPlayer()).getEmojiName();
                if (clickedItemDisplay.equalsIgnoreCase(ChatColor.YELLOW + requiredDisplay))
                    animatedEmoji.getEmojiManager().getSelectedEmoji(event.getPlayer()).run(event.getPlayer());
            }
        }
    }
}
