package com.abstractwolf.emoji.api;

import com.abstractwolf.emoji.AnimatedEmoji;
import com.abstractwolf.emoji.api.parts.EmojiPart;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class Emoji
{

    public Emoji(AnimatedEmoji plugin, String emojiName, String skullId)
    {
        this.plugin = plugin;
        this.emojiName = emojiName;
        this.skullId = skullId;
    }

    private AnimatedEmoji plugin;
    private String emojiName, skullId;

    private int playerCurrent = 0;

    private BukkitTask runnable;
    private List<EmojiPart> emojiParts = new ArrayList<>();

    public void addEmojiPart(EmojiPart part)
    {
        if (emojiParts.contains(part))
            return;

        emojiParts.add(part);
    }

    public void run(final Player player)
    {

        playerCurrent = emojiParts.size() - 1;

        runnable = new BukkitRunnable()
        {

            @Override
            public void run()
            {
                if (playerCurrent == 0)
                {
                    player.getInventory().setHelmet(null);
                    cancel();
                    return;
                }

                emojiParts.get(playerCurrent).build(player);
                playerCurrent--;
            }
        }.runTaskTimer(plugin, 0L, 10);
    }

    public ItemStack getOriginalHead()
    {
        return emojiParts.get(emojiParts.size() - 1).getItemStack();
    }

    public String getEmojiName()
    {
        return emojiName;
    }

    public List<EmojiPart> getEmojiParts()
    {
        return emojiParts;
    }

    public String getSkullId()
    {
        return skullId;
    }
}
