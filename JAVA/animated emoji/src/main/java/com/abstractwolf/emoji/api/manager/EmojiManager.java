package com.abstractwolf.emoji.api.manager;

import com.abstractwolf.emoji.api.Emoji;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class EmojiManager
{

    private HashMap<UUID, Emoji> currentEmoji = new HashMap<>();
    private HashSet<Emoji> emojiHashSet = new HashSet<>();

    /**
     * Assign an emoji to a player (plays animation)
     * @param player
     * @param emoji
     */
    public void setCurrentEmoji(Player player, String emoji)
    {
        if (currentEmoji.containsKey(player.getUniqueId()))
            currentEmoji.remove(player.getUniqueId());

        currentEmoji.put(player.getUniqueId(), getEmojiByName(emoji));
        System.out.println("Emoji: " + emoji + " enabled!");
    }

    /**
     * Add an emoji to the server, should be ran in the onEnable!
     * @param emoji
     */
    public void addEmoji(Emoji emoji)
    {
        emojiHashSet.add(emoji);
    }

    /**
     * Get an Emoji by its string name.
     * @param name
     * @return
     */
    public Emoji getEmojiByName(String name)
    {

        for (Emoji emoji : emojiHashSet)
            if (emoji.getEmojiName().equalsIgnoreCase(name))
                return emoji;

        return null;
    }

    /**
     * Get the players selected Emoji.
     * @param player
     * @return Emoji
     */
    public Emoji getSelectedEmoji(Player player)
    {
        return currentEmoji.get(player.getUniqueId());
    }

    /**
     * Check if a player has an emoji selected.
     * @param player
     * @return boolean
     */
    public boolean playerHasEmojiSelected(Player player)
    {
        return currentEmoji.get(player.getUniqueId()) != null;
    }

    /*
        Not really needed for Developers.
     */

    public HashMap<UUID, Emoji> getCurrentEmoji()
    {
        return currentEmoji;
    }

    public HashSet<Emoji> getEmojiHashSet()
    {
        return emojiHashSet;
    }
}
