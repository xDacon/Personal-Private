package com.abstractwolf.presents;

import com.abstractwolf.presents.database.Repository;
import com.abstractwolf.presents.playerdata.PlayerManager;
import com.abstractwolf.presents.presents.PresentManager;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class PresentAPI
{

    private Repository repository;
    private PlayerManager playerManager;
    private PresentManager presentManager;

    /**
     * Can be called to start up the database connection. ONLY CALL ONCE!
     * @param host
     * @param port
     * @param database
     * @param username
     * @param password
     */
    public void loadRepository(String host, int port, String database, String username, String password)
    {
        repository = new Repository(host, port, database, username, password);
    }

    /**
     * Gets the only instance there should be of the database!
     * @return Repository
     */
    public Repository getRepository()
    {
        return repository;
    }

    /**
     * Set the current present manager instance.
     * @param presentManager
     */
    public void setPresentManager(PresentManager presentManager)
    {
        this.presentManager = presentManager;
    }

    /**
     * Set the current player manager instance.
     * @param playerManager
     */
    public void setPlayerManager(PlayerManager playerManager)
    {
        this.playerManager = playerManager;
    }

    /**
     * Get the current PlayerManager instance.
     * @return PlayerManager
     */
    public PlayerManager getPlayerManager()
    {
        return playerManager;
    }

    /**
     * Get the current PresentManager instance.
     * @return
     */
    public PresentManager getPresentManager()
    {
        return presentManager;
    }
}
