package com.abstractwolf.presents.presents;

import org.bukkit.Location;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class PresentBase
{

    public PresentBase(int presentID, String presentName, Location presentLocation, String presentSkinData)
    {
        this.presentID = presentID;
        this.presentName = presentName;
        this.presentSkinData = presentSkinData;
        this.presentLocation = presentLocation;
    }

    private int presentID;
    private String presentName, presentSkinData;
    private Location presentLocation;

    /**
     * Get the Presents Unique ID!
     * @return int
     */
    public int getPresentID()
    {
        return presentID;
    }

    /**
     * Get the presents name.
     * @return String
     */
    public String getPresentName()
    {
        return presentName;
    }

    /**
     * Get the presents spawn location.
     * @return Location
     */
    public Location getPresentLocation()
    {
        return presentLocation;
    }

    /**
     * Set the current location of the present.
     * @param presentLocation
     */
    public void setPresentLocation(Location presentLocation)
    {
        this.presentLocation = presentLocation;
    }

    /**
     * Get the presents skin data.
     * @return String
     */
    public String getPresentSkinData()
    {
        return presentSkinData;
    }

    /**
     * Called when a player collects a present.
     */
    public void onCollect() {}
}
