package com.abstractwolf.presents;

import com.abstractwolf.presents.playerdata.PlayerManager;
import com.abstractwolf.presents.presents.PresentBase;
import com.abstractwolf.presents.presents.PresentManager;
import com.abstractwolf.presents.utils.LocationUtil;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class TempMain extends JavaPlugin implements Listener
{

    public PresentAPI api;

    @Override
    public void onEnable()
    {
        saveConfig();
        getConfig().options().copyDefaults(true);

        api = new PresentAPI();

        /*
            All of this is required!
         */
        api.loadRepository(
                getConfig().getString("host"),
                getConfig().getInt("port"),
                getConfig().getString("database"),
                getConfig().getString("username"),
                getConfig().getString("password"));

        api.setPlayerManager(new PlayerManager(api));
        api.setPresentManager(new PresentManager());

        getConfig().getConfigurationSection("presents").getKeys(false).forEach(value ->
        {
            api.getPresentManager().addPresent(new PresentBase(
                   getConfig().getInt("presents." + value + ".id"),
                   getConfig().getString("presents." + value + ".name"),
                   LocationUtil.parseStringToLocation(getConfig().getString("presents." + value + ".location")),
                   getConfig().getString("presents." + value + ".skindata")
            ));
        });

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable()
    {
        api.getRepository().closeConnectionPool();
    }

    @EventHandler
    public void onJoin(PlayerLoginEvent event)
    {
        api.getPlayerManager().addPlayer(event.getPlayer().getUniqueId(), event.getPlayer().getName());
    }

    @EventHandler
    public void onLoadedJoin(PlayerJoinEvent event)
    {
        api.getPlayerManager().getPlayer(event.getPlayer()).loadPreviousData(getConfig().getString("table"));
        api.getPlayerManager().getPlayer(event.getPlayer()).addSessionPresent(api.getPresentManager().getPresentByID(1));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        api.getPlayerManager().getPlayer(event.getPlayer()).saveData(getConfig().getString("table"));
        api.getPlayerManager().removePlayer(event.getPlayer().getUniqueId());
    }
}
