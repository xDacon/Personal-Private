package com.abstractwolf.presents.playerdata;

import com.abstractwolf.presents.PresentAPI;
import com.abstractwolf.presents.presents.PresentBase;
import com.abstractwolf.presents.utils.LocationUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class StoredPlayer
{

    public StoredPlayer(PresentAPI presentAPI, UUID uuid, String username)
    {
        this.uuid = uuid.toString();
        this.username = username;

        collectedPresents = new ArrayList<>();
        sessionCollectedPresents = new ArrayList<>();
        this.presentAPI = presentAPI;
    }

    private PresentAPI presentAPI;

    int id = -1;
    private String uuid, username;
    private List<PresentBase> sessionCollectedPresents;
    private List<PresentBase> collectedPresents;

    private String finalJSON = "";

    public void loadPreviousData(String tableName)
    {
        try
        {
            if (presentAPI.getRepository() == null)
            {
                //ERRoR - Not connected or instance no longer exists!
                return;
            }

            ResultSet set = presentAPI.getRepository().prepareStatement("SELECT * FROM " + tableName + " WHERE uuid='" + uuid + "'").executeQuery();

            while (!set.next())
                return;

            id = set.getInt("id");

            if (set.getString("found").length() < 1)
            {
                System.out.println("Nothing found in " + uuid + "'s found index!");
                return;
            }

            if (set.getString("FOUND").contains("|"))
            {
                for (String split : set.getString("FOUND").split("|"))
                {
                    JSONArray foundArray = new JSONArray(split);

                    int id = foundArray.getInt(0);
                    String name = foundArray.getString(1);
                    String location = foundArray.getString(2);
                    String skinData = foundArray.getString(3);

                    System.out.println(name);
                    System.out.println(location);

                    addCollectedPresent(new PresentBase(id, name, LocationUtil.parseStringToLocation(location), skinData));
                }
            }
            else
            {
                JSONArray foundArray = new JSONArray(set.getString("FOUND"));

                int id = foundArray.getInt(0);
                String name = foundArray.getString(1);
                String location = foundArray.getString(2);
                String skinData = foundArray.getString(3);

                System.out.println(name);
                System.out.println(location);

                addCollectedPresent(new PresentBase(id, name, LocationUtil.parseStringToLocation(location), skinData));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void saveData(String tableName)
    {

        String partialJSON = "";
        String finalJSON = "";

        for (PresentBase session : sessionCollectedPresents)
        {
            partialJSON = "[\"" + session.getPresentID() + "\",\"" + session.getPresentName() + "\",\"" + LocationUtil.parseLocationToString(session.getPresentLocation()) + "\",\"" + session.getPresentSkinData() + "\"]";
            finalJSON = (finalJSON.length() < 1 ? partialJSON : finalJSON + "|" + partialJSON);
            System.out.println(finalJSON);
        }

        try
        {
            if (id == -1)
            {
                presentAPI.getRepository().prepareStatement("INSERT INTO " + tableName + "(UUID, FOUND) VALUES ('" + uuid + "','" + finalJSON + "');").executeUpdate();
            }
            else
            {
                //database.prepareStatement("UPDATE UserStorage SET RANK = '" + rank.name().toUpperCase() + "', COINS = '" + coins + "' WHERE ID = " + id + ";").executeUpdate();
                presentAPI.getRepository().prepareStatement("UPDATE " + tableName + " SET UUID = '" + uuid + "', FOUND = '" + finalJSON + "' WHERE UUID = '" + uuid + "';").executeUpdate();
            }

            System.out.println("Updated database information for: '" + uuid + "'! New FOUND value: '" + finalJSON + "'");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Returns the unique ID for a user.
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Get a loaded players uuid.
     * @return String
     */
    public String getUuid()
    {
        return uuid;
    }

    /**
     * Get a loaded players username.
     * @return String
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * Add a collected present.
     * @param present
     */
    public void addCollectedPresent(PresentBase present) { collectedPresents.add(present); }

    /**
     * Remove a collected present.
     * @param present
     */
    public void removeCollectedPresent(PresentBase present) { if(collectedPresents.contains(present)) collectedPresents.remove(present); }

    /**
     * Add a present for the current session (this is used when adding a new value to a database..)
     * @param present
     */
    public void addSessionPresent(PresentBase present) { sessionCollectedPresents.add(present); }
}
