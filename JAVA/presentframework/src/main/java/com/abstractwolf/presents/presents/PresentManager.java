package com.abstractwolf.presents.presents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class PresentManager
{

    public PresentManager()
    {
        this.loadedPresents = new ArrayList<>();
    }

    private List<PresentBase> loadedPresents;

    /**
     * Add a present to the loaded present list.
     * @param present
     */
    public void addPresent(PresentBase present) { loadedPresents.add(present); System.out.println(present.getPresentName()); }

    /**
     * Get a present by its unique number id.
     * @param id
     * @return PresentBase
     */
    public PresentBase getPresentByID(int id)
    {
        for (PresentBase present : loadedPresents)
            if (present.getPresentID() == id)
                return present;
        return null;
    }

    /**
     * Get a list of loaded presents around a map.
     * @return List<PresentBase>
     */
    public List<PresentBase> getLoadedPresents()
    {
        return loadedPresents;
    }
}
