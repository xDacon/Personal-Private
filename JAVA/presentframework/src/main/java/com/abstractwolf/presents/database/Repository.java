package com.abstractwolf.presents.database;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by ThatAbstractWolf on 2016-09-03.
 */
public class Repository
{
    private HikariDataSource connectionPool;
    private String host, database, user, pass;
    private int port;

    /**
     * Constructor for connecting to the database.
     * @param host
     * @param port
     * @param database
     * @param user
     * @param pass
     */
    public Repository(String host, int port, String database, String user, String pass)
    {

        this.host = host;
        this.port = port;
        this.port = port;
        this.database = database;
        this.user = user;
        this.pass = pass;
        System.out.println("------------------------------");
        System.out.println("HOST: " + host + " PORT: " + port + " DB: " + database + " USER: " + user + " PASS: " + pass);
        System.out.println("------------------------------");
        openConnection();
    }

    /**
     * Open a connection between the database and client.
     * WARNING: MAKE SURE TO SET THE LOGIN INFORMATION IN THE CONSTRUCTOR FOR THE CLASS!
     */
    public void openConnection()
    {
        try
        {
            connectionPool = new HikariDataSource();
            connectionPool.setMaximumPoolSize(20);
            connectionPool.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
            connectionPool.addDataSourceProperty("serverName", host);
            connectionPool.addDataSourceProperty("port", port);
            connectionPool.addDataSourceProperty("databaseName", database);
            connectionPool.addDataSourceProperty("user", user);
            connectionPool.addDataSourceProperty("password", pass);
            connectionPool.setConnectionTimeout(3000);
            connectionPool.setValidationTimeout(1000);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Send prepared statements to the server/database.
     * @param query
     * @return PreparedStatement
     */
    public PreparedStatement prepareStatement(String query)
    {

        Connection connection;
        PreparedStatement statement = null;

        try
        {
            connection = connectionPool.getConnection();
            statement = connection.prepareStatement(query);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            try
            {
                if (connectionPool.getConnection() == null || connectionPool.getConnection().isClosed()) {
                    openConnection();
                }
            }
            catch (SQLException error)
            {
                error.printStackTrace();
            }
        }

        return statement;
    }

    /**
     * Returns the database name.
     * @return String
     */
    public String getDatabase()
    {
        return database;
    }

    /**
     * Close the current connection between the database and client.
     */
    public void closeConnectionPool() { connectionPool.close(); }
}

