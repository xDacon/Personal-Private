package com.abstractwolf.presents.playerdata;

import com.abstractwolf.presents.PresentAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class PlayerManager
{

    public PlayerManager(PresentAPI api)
    {
        this.api = api;
        this.storedPlayers = new HashMap<>();
    }

    private PresentAPI api;
    private Map<UUID, StoredPlayer> storedPlayers;

    /**
     * Add a player to the storedplayer map.
     * @param uuid
     */
    public void addPlayer(UUID uuid, String username) { storedPlayers.put(uuid, new StoredPlayer(api, uuid, username)); }

    /**
     * Remove a player from the storedplayer map.
     * @param uuid
     */
    public void removePlayer(UUID uuid) { if(storedPlayers.containsKey(uuid)) storedPlayers.remove(uuid); }

    /**
     * Get a stored player by a player instance.
     * @param player
     * @return StoredPlayer
     */
    public StoredPlayer getPlayer(Player player) { return (storedPlayers.get(player.getUniqueId()) != null ? storedPlayers.get(player.getUniqueId()) : null); }
}
