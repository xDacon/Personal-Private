package com.ilvermornyschoolmc.npcdialog.manager;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import com.ilvermornyschoolmc.npcdialog.dialog.DialogNPC;
import com.ilvermornyschoolmc.npcdialog.dialog.DialogPlayer;
import com.ilvermornyschoolmc.npcdialog.dialog.Question;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class PlayerManager
{

    public PlayerManager(NPCDialog npcDialog)
    {
        this.npcDialog = npcDialog;

        dialogedUsers = new HashMap<>();
    }

    private NPCDialog npcDialog;
    private HashMap<UUID, DialogPlayer> dialogedUsers;

    public void setCurrentDialog(Player player, DialogNPC dialogNPC)
    {
        dialogedUsers.put(player.getUniqueId(), new DialogPlayer(npcDialog, player.getUniqueId()));
        getUserDialog(player).setDialogNPC(dialogNPC);
        getUserDialog(player).setCurrentQuestion(dialogNPC.getQuestions().get(0));
    }

    public void removeCurrentDialog(Player player)
    {
        if (!dialogedUsers.containsKey(player.getUniqueId()))
            return;

        dialogedUsers.remove(player.getUniqueId());
    }

    public Question getNextQuestion(Player player)
    {
        if (getUserDialog(player).getCurrentQuestion().getId() < (getUserDialog(player).getDialogNPC().getQuestions().size() + 1))
            return getUserDialog(player).getDialogNPC().getQuestions().get((getUserDialog(player).getCurrentQuestion().getId() - 1) + 1);
        return null;
    }

    public DialogPlayer getUserDialog(Player player)
    {
        return dialogedUsers.get(player.getUniqueId());
    }

    public HashMap<UUID, DialogPlayer> getDialogedUsers()
    {
        return dialogedUsers;
    }
}