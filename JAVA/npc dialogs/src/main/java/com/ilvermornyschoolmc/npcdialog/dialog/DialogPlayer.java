package com.ilvermornyschoolmc.npcdialog.dialog;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import com.ilvermornyschoolmc.npcdialog.dialog.gui.AnswerUI;
import com.ilvermornyschoolmc.npcdialog.util.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class DialogPlayer
{

    public DialogPlayer(NPCDialog npcDialog, UUID uuid)
    {
        this.npcDialog = npcDialog;
        this.uuid = uuid;
    }

    private NPCDialog npcDialog;
    private UUID uuid;

    private DialogNPC dialogNPC;
    private Question currentQuestion;

    public UUID getUuid()
    {
        return uuid;
    }

    public Question getCurrentQuestion()
    {
        return currentQuestion;
    }

    public DialogNPC getDialogNPC()
    {
        return dialogNPC;
    }

    public void setCurrentQuestion(Question currentQuestion)
    {
        this.currentQuestion = currentQuestion;
    }

    public void confirmAnswer(Answer answer)
    {
        Player player = Bukkit.getPlayer(uuid);
        //TODO message for confirm, dialog from entity and give a reward, next question
        ChatUtil.sendMessage(player, "Answer", "Your answer was confirmed as, &b'" + answer.getAnswer().split(";")[0] + "'");

        try { ChatUtil.sendMessage(player, "Response", answer.getAnswer().split(";")[1]);}
        catch (IndexOutOfBoundsException e) {}

        try
        {
            setCurrentQuestion(npcDialog.getPlayerManager().getNextQuestion(player));
            new AnswerUI(npcDialog, player, currentQuestion);
        }
        catch (IndexOutOfBoundsException e)
        {
            npcDialog.getPlayerManager().removeCurrentDialog(player);
            ChatUtil.sendMessage(player, "Dev", "You have answered all questions!");
            player.closeInventory();
        }
    }

    public void setDialogNPC(DialogNPC dialogNPC)
    {
        this.dialogNPC = dialogNPC;
    }
}
