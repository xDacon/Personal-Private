package com.ilvermornyschoolmc.npcdialog.dialog;

import net.citizensnpcs.api.npc.NPC;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class Question
{

    public Question(int id, NPC originalNPC, String question)
    {
        this.id = id;
        this.originalNPC = originalNPC;
        this.question = question;

        answers = new ArrayList<>();
    }

    private int id;
    private NPC originalNPC;
    private String question;
    private List<Answer> answers;

    public int getId()
    {
        return id;
    }

    public NPC getOriginalNPC()
    {
        return originalNPC;
    }

    public List<Answer> getAnswers()
    {
        return answers;
    }

    public String getQuestion()
    {
        return question;
    }

    public void addAnswer(Answer answer)
    {
        if (answers.contains(answer))
            return;

        answers.add(answer);
    }

    public boolean answerExists(int answerID)
    {
        for (Answer answers : this.answers)
            if (answers.getId() == answerID)
                return true;

        return false;
    }
}
