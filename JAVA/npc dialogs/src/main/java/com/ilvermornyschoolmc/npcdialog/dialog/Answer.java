package com.ilvermornyschoolmc.npcdialog.dialog;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class Answer
{

    public Answer(int id, Question originalQuestion, String answer)
    {
        this.id = id;
        this.originalQuestion = originalQuestion;
        this.answer = answer;
    }

    private int id;
    private Question originalQuestion;
    private String answer;

    public int getId()
    {
        return id;
    }

    public Question getOriginalQuestion()
    {
        return originalQuestion;
    }

    public String getAnswer()
    {
        return answer;
    }
}