package com.ilvermornyschoolmc.npcdialog.manager;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import com.ilvermornyschoolmc.npcdialog.dialog.Answer;
import com.ilvermornyschoolmc.npcdialog.dialog.DialogNPC;
import com.ilvermornyschoolmc.npcdialog.dialog.Question;
import com.ilvermornyschoolmc.npcdialog.util.LocationUtil;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class DialogManager
{

    public DialogManager(NPCDialog npcDialog)
    {
        this.npcDialog = npcDialog;

        dialogNPCs = new ArrayList<>();
        clearNPCS = new ArrayList<>();
    }

    private NPCDialog npcDialog;

    private List<NPC> clearNPCS;
    private List<DialogNPC> dialogNPCs;

    private int questionId = 1, answerId = 1;

    public void loadNPCS()
    {

        /*
        npcs:
          'TestNPC':
            location: world;0;150;0;0;0
            questions:
              'TestQuestion1':
                1: ''
                2: ''
                3: ''
                4: ''
              'TestQuestion2':
                1: 'Test1'
                2: 'Test2'
         */

        questionId = 1;

        npcDialog.getConfig().getConfigurationSection("npcs").getKeys(false).forEach(key ->
        {
            DialogNPC dialog = new DialogNPC(key, EntityType.valueOf(npcDialog.getConfig().getString("npcs." + key + ".entity")), LocationUtil.parseStringToLocation(npcDialog.getConfig().getString("npcs." + key + ".location")));
            dialog.buildNPC();
            clearNPCS.add(dialog.getNpc());

            npcDialog.getConfig().getConfigurationSection("npcs." + key + ".questions").getKeys(false).forEach(question ->
            {
                answerId = 1;

                Question questions = new Question(questionId, dialog.getNpc(), question);

                npcDialog.getConfig().getStringList("npcs." + key + ".questions." + question).forEach(answer ->
                {
                    questions.addAnswer(new Answer(answerId, questions, answer));
                    System.out.println("Added new answer: " + answer + " to question " + questions.getId());
                    answerId += 1;
                });

                dialog.addQuestion(questions);
                questionId += 1;
            });

            addDialogNPC(dialog);
            System.out.println("Added a new DialogNPC with: '" + dialog.getQuestions().size() + "' questions.");
        });
    }

    public void addDialogNPC(DialogNPC dialogNPC)
    {
        if (dialogNPCs.contains(dialogNPC))
            return;

        dialogNPCs.add(dialogNPC);
    }

    public DialogNPC getDialogNPC(String dialogName)
    {
        for (DialogNPC npcs : dialogNPCs)
            if (npcs.getDialogName().equals(dialogName))
                return npcs;

        return null;
    }

    public List<DialogNPC> getDialogNPCs()
    {
        return dialogNPCs;
    }

    public List<NPC> getClearNPCS()
    {
        return clearNPCS;
    }
}
