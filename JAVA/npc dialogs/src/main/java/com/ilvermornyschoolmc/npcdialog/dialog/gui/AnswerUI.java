package com.ilvermornyschoolmc.npcdialog.dialog.gui;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import com.ilvermornyschoolmc.npcdialog.dialog.Answer;
import com.ilvermornyschoolmc.npcdialog.dialog.Question;
import com.ilvermornyschoolmc.npcdialog.util.ItemBuilder;
import com.ilvermornyschoolmc.npcdialog.util.menu.Menu;
import com.ilvermornyschoolmc.npcdialog.util.menu.MenuItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class AnswerUI extends Menu
{

    public AnswerUI(NPCDialog npcDialog, Player player, Question question)
    {
        super(npcDialog, "Answers - " + question.getQuestion(), 6);

        int slot = 0;

        for (Answer answers : question.getAnswers())
        {
            addItem(new MenuItem(slot, new ItemBuilder(Material.STAINED_CLAY, (byte) 5)
                    .setName("&b" + answers.getAnswer().split(";")[0])
                    .setLore(ChatColor.GRAY + "Click to confirm answer.")
                    .build())
            {
                @Override
                public void click(Player player, ClickType clickType)
                {
                    npcDialog.getPlayerManager().getUserDialog(player).confirmAnswer(answers);
                }
            });
            slot += 1;
        }

        openInventory(player);
    }
}
