package com.ilvermornyschoolmc.npcdialog;

import com.ilvermornyschoolmc.npcdialog.listener.QuestionListener;
import com.ilvermornyschoolmc.npcdialog.manager.DialogManager;
import com.ilvermornyschoolmc.npcdialog.manager.PlayerManager;
import com.ilvermornyschoolmc.npcdialog.util.menu.MenuManager;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class NPCDialog extends JavaPlugin
{

    private DialogManager dialogManager;
    private PlayerManager playerManager;

    @Override
    public void onEnable()
    {
        dialogManager = new DialogManager(this);
        playerManager = new PlayerManager(this);

        if (!new File(getDataFolder().getAbsolutePath() + "/config.yml").exists())
            getConfig().options().copyDefaults(true);

        saveConfig();

        dialogManager.loadNPCS();

        Bukkit.getPluginManager().registerEvents(new MenuManager(this), this);
        Bukkit.getPluginManager().registerEvents(new QuestionListener(this), this);
    }

    @Override
    public void onDisable()
    {
        dialogManager.getClearNPCS().forEach(npc ->
        {
            CitizensAPI.getNPCRegistry().deregister(npc);
            npc.destroy();
        });

        /* Stops memory leaks.. */
        dialogManager.getDialogNPCs().clear();
        dialogManager.getClearNPCS().clear();
        playerManager.getDialogedUsers().clear();
    }

    public DialogManager getDialogManager()
    {
        return dialogManager;
    }

    public PlayerManager getPlayerManager()
    {
        return playerManager;
    }
}