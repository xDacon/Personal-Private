package com.ilvermornyschoolmc.npcdialog.util;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class ChatUtil
{

    public static void sendMessage(Player player, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + message.toString()));
    }

    public static void sendMessage(Player player, Object prefix, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c" + prefix.toString() + " &8>> &7" + message.toString()));
    }

    public static String convertUppercase(String str) { return str.substring(0, 1).toUpperCase() + str.substring(1, str.length()).toLowerCase(); }
}