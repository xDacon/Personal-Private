package com.ilvermornyschoolmc.npcdialog.listener;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import com.ilvermornyschoolmc.npcdialog.dialog.gui.AnswerUI;
import com.ilvermornyschoolmc.npcdialog.util.ChatUtil;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class QuestionListener implements Listener
{

    public QuestionListener(NPCDialog npcDialog)
    {
        this.npcDialog = npcDialog;
    }

    private NPCDialog npcDialog;

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event)
    {
        if (event.getPlayer() instanceof Player)
        {
            Player player = (Player) event.getPlayer();

            if (npcDialog.getPlayerManager().getUserDialog(player) != null)
            {
                new BukkitRunnable()
                {
                    @Override
                    public void run()
                    {
                        if (player.getOpenInventory() == null || !player.getOpenInventory().getTitle().equals("Answers - " + npcDialog.getPlayerManager().getUserDialog(player).getCurrentQuestion().getQuestion()))
                        {
                            ChatUtil.sendMessage(player, "Dialog", "You have decided to exit the dialog!");
                            npcDialog.getPlayerManager().removeCurrentDialog(player);
                        }
                    }
                }.runTaskLater(npcDialog, 10);
            }
        }
    }

    @EventHandler
    public void onInteract(NPCRightClickEvent event)
    {
        Player player = event.getClicker();
        NPC npc = event.getNPC();

        if (npcDialog.getPlayerManager().getUserDialog(player) != null)
        {
            ChatUtil.sendMessage(player, "Dialog", "You are already talking to another npc. Are you cheating?!");
            return;
        }

        if (npcDialog.getDialogManager().getDialogNPC(npc.getName()) != null)
        {
            //TODO exists
            npcDialog.getPlayerManager().setCurrentDialog(player, npcDialog.getDialogManager().getDialogNPC(npc.getName()));
            new AnswerUI(npcDialog, player, npcDialog.getDialogManager().getDialogNPC(npc.getName()).getQuestions().get(0));
        }
    }
}
