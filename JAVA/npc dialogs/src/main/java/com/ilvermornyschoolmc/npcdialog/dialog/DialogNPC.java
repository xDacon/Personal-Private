package com.ilvermornyschoolmc.npcdialog.dialog;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2017-01-08.
 */
public class DialogNPC
{

    public DialogNPC(String dialogName, EntityType entityType, Location spawnLocation)
    {
        this.dialogName = dialogName;
        this.entityType = entityType;
        this.spawnLocation = spawnLocation;

        questions = new ArrayList<>();
    }

    private String dialogName;
    private EntityType entityType;
    private Location spawnLocation;
    private NPC npc;

    private List<Question> questions;

    public NPC getNpc()
    {
        return npc;
    }

    public String getDialogName()
    {
        return dialogName;
    }

    public EntityType getEntityType()
    {
        return entityType;
    }

    public Location getSpawnLocation()
    {
        return spawnLocation;
    }

    public List<Question> getQuestions()
    {
        return questions;
    }

    public void addQuestion(Question question)
    {
        if (questions.contains(question))
            return;

        questions.add(question);
    }

    public void buildNPC()
    {
        npc = CitizensAPI.getNPCRegistry().createNPC(entityType, dialogName);
        npc.spawn(spawnLocation);

        System.out.println("NPC generated for: " + dialogName + " at location: '" + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ() + "'");
    }
}
