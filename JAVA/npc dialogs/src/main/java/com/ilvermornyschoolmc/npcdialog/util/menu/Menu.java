package com.ilvermornyschoolmc.npcdialog.util.menu;

import java.util.ArrayList;
import java.util.List;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class Menu implements InventoryHolder
{

	protected NPCDialog npcDialog;

	private final Inventory inventory;

	private final String title;
	private final int rows;

	private List<MenuItem> items;

	public Menu(NPCDialog npcDialog, String title, int rows)
	{
		this.npcDialog = npcDialog;
		this.title = title;
		this.rows = rows;

		items = new ArrayList<>();
		inventory = Bukkit.createInventory(this, (rows * 9), ChatColor.translateAlternateColorCodes('&', title));
	}

	public void addItem(MenuItem item)
	{
		items.add(item);
	}

	public void openInventory(Player player)
	{
		if (inventory == null)
			return;

		inventory.clear();

		for (MenuItem item : items)
			inventory.setItem(item.getIndex(), item.getItemStack());

		player.openInventory(inventory);
	}

	@Override
	public Inventory getInventory()
	{
		return inventory;
	}

	public String getTitle()
	{
		return title;
	}

	public int getRows()
	{
		return rows;
	}
	public List<MenuItem> getItems()
	{
		return items;
	}
}
