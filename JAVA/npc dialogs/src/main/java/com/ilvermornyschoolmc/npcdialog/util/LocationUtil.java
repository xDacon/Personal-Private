package com.ilvermornyschoolmc.npcdialog.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class LocationUtil
{

    /**
     * Parses a location in string format in the format of: x;y;z;yaw;pitch
     * @param location
     * @return String
     */
    public static String parseLocationToString(Location location) { return (int) location.getX() + ";" + (int) location.getY() + ";" + (int) location.getZ() + ";" + location.getYaw() + ";" + location.getPitch(); }

    /**
     * Converts a string in the format: x;y;z;yaw;pitch to a location.
     * @param location
     * @return Location
     */
    public static Location parseStringToLocation(String location)
    {
        String[] parsed = location.split(";");
        World world = Bukkit.getWorld(parsed[0]);
        double x = Double.parseDouble(parsed[1]), y = Double.parseDouble(parsed[2]), z = Double.parseDouble(parsed[3]);
        float yaw = Float.parseFloat(parsed[4]), pitch = Float.parseFloat(parsed[5]);
        return new Location(world, x, y, z, yaw, pitch);
    }
}