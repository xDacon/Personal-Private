package com.ilvermornyschoolmc.npcdialog.util.menu;

import com.ilvermornyschoolmc.npcdialog.NPCDialog;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

/**
 * Created by ThatAbstractWolf on 2016-12-20.
 */
public class MenuManager implements Listener
{

	public MenuManager(NPCDialog npcDialog)
	{
		this.npcDialog = npcDialog;
	}

	private NPCDialog npcDialog;

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getInventory() == null) 
			return;

		InventoryHolder holder = event.getInventory().getHolder();
		
		if (holder == null) 
			return;

		if (!(holder instanceof Menu))
			return;

		event.setCancelled(true);
		
		if (!(event.getWhoClicked() instanceof Player)) 
			return;

		Player player = (Player) event.getWhoClicked();
		Menu baylorMenu = (Menu) holder;
		ItemStack clicked = event.getCurrentItem();

		ClickType clickType = event.getClick();

		if (clicked == null || clicked.getType() == Material.AIR)
			return;

		if (clickType == null) 
			return;

		for (MenuItem menuItem : baylorMenu.getItems())
		{
			if (!menuItem.getItemStack().equals(clicked))
				continue;

			menuItem.click(player, clickType);
			break;
		}
	}
}