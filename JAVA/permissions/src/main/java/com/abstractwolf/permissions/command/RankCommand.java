package com.abstractwolf.permissions.command;

import com.abstractwolf.permissions.Permissions;
import com.abstractwolf.permissions.base.Rank;
import com.abstractwolf.permissions.util.ChatUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class RankCommand implements CommandExecutor
{

    public RankCommand(Permissions permissions)
    {
        this.permissions = permissions;
    }

    private Permissions permissions;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {

        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            if (player.hasPermission("permissions.admin"))
            {
                if (args.length == 2)
                {
                    if (args[0].equalsIgnoreCase("check"))
                    {
                        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);

                        if (!target.isOnline())
                        {
                            ChatUtil.sendMessage(player, "Rank", "&4" + args[1] + " &7is offline! Checking rank now..");
                            new BukkitRunnable()
                            {
                                @Override
                                public void run()
                                {
                                    try
                                    {
                                        ResultSet set = permissions.getRepository().prepareStatement("SELECT * FROM data WHERE UUID = '" + target.getUniqueId().toString() + "';").executeQuery();

                                        if (!set.next())
                                            return;

                                        ChatUtil.sendMessage(player, "Rank", "&4" + args[1] + "'s &7rank is &4" + set.getString("rank"));
                                    }
                                    catch (SQLException e)
                                    {
                                        ChatUtil.sendMessage(player, "Error", "Are you sure this player has joined before?");
                                    }
                                }
                            }.runTaskAsynchronously(permissions);
                            return false;
                        }

                        ChatUtil.sendMessage(player, "Rank", "&4" + args[1] + "'s &7rank is &4" + permissions.getPlayerManager().getPlayer(target.getPlayer()).getRank().getRankName());
                    }
                }
                else if (args.length == 3)
                {
                    if (args[0].equalsIgnoreCase("set"))
                    {
                        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
                        Rank rank = permissions.getRankManager().getRank(args[2]);

                        if (rank == null)
                        {
                            ChatUtil.sendMessage(player, "Rank", "&4" + args[2] + " &7is not a valid rank in the permissions.yml!");
                            return false;
                        }

                        if (!target.isOnline())
                        {
                            ChatUtil.sendMessage(player, "Rank", "&4" + args[1] + " &7is offline! Updating offline");

                            new BukkitRunnable()
                            {
                                @Override
                                public void run()
                                {
                                    try
                                    {
                                        permissions.getRepository().prepareStatement("UPDATE data SET RANK = '" + rank.getConfigName().toUpperCase() + "' WHERE UUID = '" + target.getUniqueId().toString() + "';").executeUpdate();

                                        if (!player.getName().equalsIgnoreCase(target.getName()))
                                            ChatUtil.sendMessage(player, "Rank", "&4" + args[1] + "'s &7rank is now &4" + rank.getRankName());
                                    }
                                    catch (SQLException e)
                                    {
                                        e.printStackTrace();
                                        ChatUtil.sendMessage(player, "Error", "There was an error updating the rank! Has this player joined before?");
                                    }
                                }
                            }.runTaskAsynchronously(permissions);
                            return false;
                        }

                        permissions.getPlayerManager().getPlayer(target.getPlayer()).setRank(rank);
                        permissions.getPlayerManager().getPlayer(target.getPlayer()).saveData();
                        target.getPlayer().kickPlayer(ChatColor.translateAlternateColorCodes('&', "&7Your rank has been updated to &4" + rank.getRankName()));

                        if (!player.getName().equalsIgnoreCase(target.getName()))
                            ChatUtil.sendMessage(player, "Rank", "&4" + args[1] + "'s &7rank is now &4" + rank.getRankName());
                    }
                }
                else
                {
                    ChatUtil.sendMessage(player, "Help", "Permission Help");
                    ChatUtil.sendMessage(player, "&f- &7/rank check <name> - offline support");
                    ChatUtil.sendMessage(player, "&f- &7/rank set <name> <rank> - offline support");
                }
            }
            else
            {
                ChatUtil.sendMessage(player, "Rank", "You do not have permission to use this command! &4permissions.admin &7is required!");
            }
        }
        return false;
    }
}
