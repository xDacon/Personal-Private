package com.abstractwolf.permissions.manager;

import com.abstractwolf.permissions.Permissions;
import com.abstractwolf.permissions.base.PermissionPlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class PlayerManager
{

    public PlayerManager(Permissions permissions)
    {
        this.permissions = permissions;
        this.players = new HashMap<>();
    }

    private Permissions permissions;
    private HashMap<UUID, PermissionPlayer> players;

    public void addPlayer(UUID uuid)
    {

        if (!players.containsKey(uuid))
        {
            players.put(uuid, new PermissionPlayer(permissions, uuid));
            System.out.println("Added player: '" + uuid + "'.");
        }
    }

    public void loadAttachment(Player player)
    {
        PermissionAttachment attachment = player.addAttachment(permissions);
        players.get(player.getUniqueId()).setPermissionAttachment(attachment);
    }

    public void removePlayer(Player player)
    {

        if (players.containsKey(player.getUniqueId()))
        {
            players.remove(player.getUniqueId());
            System.out.println("Removed player: '" + player.getName() + "'.");
        }
    }

    public PermissionPlayer getPlayer(UUID player)
    {
        return players.get(player);
    }

    public PermissionPlayer getPlayer(Player player)
    {
        return players.get(player.getUniqueId());
    }
}
