package com.abstractwolf.permissions.base;

import com.abstractwolf.permissions.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class PermissionPlayer
{

    public PermissionPlayer(Permissions permissions, UUID uuid)
    {
        this.uuid = uuid;
        this.permissions = permissions;
    }

    private Permissions permissions;

    private PermissionAttachment permissionAttachment;

    private int id = -1;
    private UUID uuid;
    private Rank rank;

    public void loadData()
    {

        new BukkitRunnable()
        {
            @Override
            public void run()
            {

                try
                {

                    ResultSet set = permissions.getRepository().prepareStatement("SELECT * FROM data WHERE UUID = '" + uuid.toString() + "';").executeQuery();

                    if (!set.next())
                        return;

                    id = set.getInt("ID");

                    if (permissions.getRankManager().getRank(set.getString("rank")) == null)
                    {
                        rank = permissions.getRankManager().getDefaultRank();
                    }
                    else
                    {
                        rank = permissions.getRankManager().getRank(set.getString("rank"));
                    }

                    System.out.println("-------------------------[Live Data]-------------------------");
                    System.out.println("ID: " + id + " UUID: " + uuid.toString() + " Username: " + (Bukkit.getPlayer(uuid) == null ? "'Could not collect'" : Bukkit.getPlayer(uuid).getName()) +  " Rank: " + rank.getConfigName());
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }

            }
        }.runTaskAsynchronously(permissions);
    }

    public void saveData()
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {

                if (id == -1)
                {

                    try
                    {
                        permissions.getRepository().prepareStatement("INSERT INTO data (UUID, RANK) VALUES ('" + uuid.toString() + "','" + (rank == null ? permissions.getRankManager().getDefaultRank().getConfigName().toUpperCase() : rank.getConfigName().toUpperCase()) + "');").executeUpdate();
                    }
                    catch (SQLException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {

                    try
                    {
                        permissions.getRepository().prepareStatement("UPDATE data SET RANK = '" + (rank == null ? permissions.getRankManager().getDefaultRank().getConfigName().toUpperCase() : rank.getConfigName().toUpperCase())  + "' WHERE ID = " + id + ";").executeUpdate();
                    }
                    catch (SQLException e)
                    {
                        e.printStackTrace();
                    }
                }

            }
        }.runTaskAsynchronously(permissions);
    }

    public int getId()
    {
        return id;
    }

    /**
     * Get a players uuid.
     * @return
     */
    public UUID getUUID()
    {
        return uuid;
    }

    /**
     * Get the players permission attachment for adding permissions.
     * @return PermissionAttachment
     */
    public PermissionAttachment getPermissionAttachment()
    {
        return permissionAttachment;
    }

    /**
     * Set the players current rank.
     * @param rank
     */
    public void setRank(Rank rank)
    {
        this.rank = rank;
    }

    /**
     * Players current rank from the database.
     * @return
     */
    public Rank getRank()
    {
        return rank;
    }

    public void setPermissionAttachment(PermissionAttachment permissionAttachment) { this.permissionAttachment = permissionAttachment; }
}
