package com.abstractwolf.permissions.configuration;

import com.abstractwolf.permissions.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class PermissionsConfiguration
{

    public PermissionsConfiguration(Permissions permissions)
    {
        this.permissions = permissions;
    }

    private Permissions permissions;

    private FileConfiguration permissionsConfig;
    private File permissionsFile;

    public void reloadConfig()
    {
        permissionsFile = new File(permissions.getDataFolder(), "permissions.yml");

        if (!permissionsFile.exists())
        {
            try
            {
                permissionsFile.createNewFile();
            }
            catch (IOException e)
            {
                System.out.println("Could not create permissions.yml!");
            }
        }

        permissionsConfig = YamlConfiguration.loadConfiguration(permissionsFile);
    }

    public void saveConfig()
    {
        try
        {
            permissionsConfig.save(permissionsFile);
        }
        catch (IOException e)
        {
            System.out.println("Could not save permissions.yml.");
        }
    }

    public FileConfiguration getConfig()
    {
        return permissionsConfig;
    }

}
