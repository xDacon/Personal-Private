package com.abstractwolf.permissions.listener;

import com.abstractwolf.permissions.Permissions;
import com.abstractwolf.permissions.util.packets.TabPacket;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class ConnectionListener implements Listener
{

    public ConnectionListener(Permissions permissions)
    {
        this.permissions = permissions;
    }

    private Permissions permissions;
    @EventHandler
    public void onConnect(AsyncPlayerPreLoginEvent event)
    {

        if (permissions.getRepository().isClosed())
            permissions.getRepository().openConnection();

        permissions.getPlayerManager().addPlayer(event.getUniqueId());
    }

    @EventHandler
    public void onConnectFully(PlayerJoinEvent event)
    {

        permissions.getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).loadData();

        new BukkitRunnable()
        {
            @Override
            public void run()
            {

                if (permissions.getPlayerManager().getPlayer(event.getPlayer()) == null)
                    return;

                permissions.getPlayerManager().loadAttachment(event.getPlayer());
                permissions.getPermissionManager().loadRankPermissions(event.getPlayer());

            }
        }.runTaskLater(permissions, 2 * 20);
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event)
    {

        if (permissions.getPlayerManager().getPlayer(event.getPlayer()).getPermissionAttachment() != null)
            permissions.getPermissionManager().wipePermissions(event.getPlayer());

        if (permissions.getPlayerManager().getPlayer(event.getPlayer()).getId() == -1)
            permissions.getPlayerManager().getPlayer(event.getPlayer()).saveData();

        permissions.getPlayerManager().removePlayer(event.getPlayer());
    }
}
