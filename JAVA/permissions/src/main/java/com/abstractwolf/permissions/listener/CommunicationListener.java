package com.abstractwolf.permissions.listener;

import com.abstractwolf.permissions.Permissions;
import com.abstractwolf.permissions.base.PermissionPlayer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class CommunicationListener implements Listener
{

    public CommunicationListener(Permissions permissions)
    {
        this.permissions = permissions;
    }

    private Permissions permissions;

    @EventHandler
    public void onConnect(AsyncPlayerChatEvent event)
    {

        if (permissions.getPlayerManager().getPlayer(event.getPlayer()) != null)
        {
            PermissionPlayer permissionPlayer = permissions.getPlayerManager().getPlayer(event.getPlayer());
            event.setFormat(ChatColor.translateAlternateColorCodes('&', permissions.getConfig().getString("chat.format").replace("{prefix}", permissionPlayer.getRank().getPrefix()).replace("{player}", event.getPlayer().getName())) + " " + event.getMessage());
        }
    }
}
