package com.abstractwolf.permissions.util.packets.reflection;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class Reflection
{

	
	public static void sendPackets(Player player, Object packet)
	{
		try
		{
			Object handle = player.getClass().getMethod("getHandle").invoke(player);
			Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
			playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConnection, packet);
		}
		catch (IllegalAccessException | InvocationTargetException | IllegalArgumentException | NoSuchMethodException | NoSuchFieldException | SecurityException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Class<?> getNMSClass(String name) 
	{
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		
		try
		{
			return Class.forName("net.minecraft.server." + version + "." + name);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static Class<?> getBukkitClass(String name) 
	{
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		
		try
		{
			return Class.forName("org.bukkit.craftbukkit." + version + "." + name);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
