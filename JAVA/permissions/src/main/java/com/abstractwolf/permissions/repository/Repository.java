package com.abstractwolf.permissions.repository;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class Repository
{

    private HikariDataSource connectionPool;
    private String host, database, user, pass;
    private int port;

    /**
     * Connect to the repository.. (MySQL)
     * @param host
     * @param port
     * @param database
     * @param user
     * @param pass
     */
    public Repository(String host, int port, String database, String user, String pass)
    {
        this.host = host;
        this.port = port;
        this.database = database;
        this.user = user;
        this.pass = pass;
        System.out.println("------------------------------[Connection Information]------------------------------");
        System.out.println("HOST: " + host + " PORT: " + port + " DB: " + database + " USER: " + user + " PASS: " + pass);
        System.out.println("------------------------------[Connection Information]------------------------------");
        openConnection();
    }

    /**
     * Connect to the repository.. (MySQL)
     */
    public void openConnection()
    {
        try
        {
            connectionPool = new HikariDataSource();
            connectionPool.setMaximumPoolSize(20);
            connectionPool.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
            connectionPool.addDataSourceProperty("serverName", host);
            connectionPool.addDataSourceProperty("port", port);
            connectionPool.addDataSourceProperty("databaseName", database);
            connectionPool.addDataSourceProperty("user", user);
            connectionPool.addDataSourceProperty("password", pass);
            connectionPool.setConnectionTimeout(3000);
            connectionPool.setValidationTimeout(1000);
            System.out.println("Opened connection between database and server!");
            System.out.println("IP: " + host + " PORT: " + port);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Error here.. you can guess where..");
        }
    }

    /**
     * Send queries to the database (Prepared Statement)
     * @param query
     * @return PreparedStatement
     */
    public PreparedStatement prepareStatement(String query)
    {

        Connection connection;
        PreparedStatement statement = null;

        try
        {
            connection = connectionPool.getConnection();
            statement = connection.prepareStatement(query);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            try
            {
                if (connectionPool.getConnection() == null || connectionPool.getConnection().isClosed()) {
                    openConnection();
                }
            }
            catch (SQLException error)
            {
                error.printStackTrace();
            }
        }

        return statement;
    }

    public boolean isClosed() { return connectionPool.isClosed(); }

    /**
     * Close Connection Pool
     */
    public void closeConnectionPool() { connectionPool.close(); }
}