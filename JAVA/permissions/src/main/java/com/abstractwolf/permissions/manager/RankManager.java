package com.abstractwolf.permissions.manager;

import com.abstractwolf.permissions.base.Rank;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class RankManager
{

    public RankManager()
    {
        ranks = new ArrayList<>();
    }

    private List<Rank> ranks;

    public void addRank(Rank rank)
    {
        ranks.add(rank);
    }

    public Rank getRank(String name)
    {
        for (Rank rank : ranks)
            if (name.equalsIgnoreCase(rank.getConfigName()) || name.equalsIgnoreCase(rank.getRankName()))
                return rank;

        return null;
    }

    public Rank getDefaultRank()
    {
        for (Rank rank : ranks)
            if (rank.isDefaultRank())
                return rank;

        return null;
    }

    public List<Rank> getRanks()
    {
        return ranks;
    }
}
