package com.abstractwolf.permissions.base;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class Rank
{

    public Rank(String rankName, String configName, String prefix, boolean defaultRank)
    {
        this.rankName = rankName;
        this.configName = configName;
        this.prefix = prefix;
        this.defaultRank = defaultRank;
    }

    private String rankName;
    private String configName;
    private String prefix;
    private boolean defaultRank;

    /**
     * Get the ranks overall name.
     * @return String
     */
    public String getRankName()
    {
        return rankName;
    }

    /**
     * Get the configuration name, this is the value in the config. ('DEV', 'ADMIN' ETC)
     * @return String
     */
    public String getConfigName()
    {
        return configName;
    }

    /**
     * Get the prefix for the rank.
     * @return String
     */
    public String getPrefix()
    {
        return prefix;
    }

    /**
     * Is this rank the default one for the server you're on.
     * @return boolean
     */
    public boolean isDefaultRank()
    {
        return defaultRank;
    }
}