package com.abstractwolf.permissions.manager;

import com.abstractwolf.permissions.Permissions;
import com.abstractwolf.permissions.base.Rank;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class PermissionManager
{

    public PermissionManager(Permissions permissions)
    {
        this.permissions = permissions;
    }

    private Permissions permissions;

    /**
     * Add a permission to a specific player.
     * @param player
     * @param permission
     */
    public void addPermission(Player player, String permission)
    {

        if (permissions.getPlayerManager().getPlayer(player) == null)
        {
            System.out.println(player.getName() + " has not been assigned a permission attachment!");
            return;
        }

        permissions.getPlayerManager().getPlayer(player).getPermissionAttachment().setPermission(new Permission(permission), true);
    }

    /**
     * Remove a permission from a specific player.
     * @param player
     * @param permission
     */
    public void removePermission(Player player, String permission)
    {

        if (permissions.getPlayerManager().getPlayer(player) == null)
        {
            System.out.println(player.getName() + " has not been assigned a permission attachment!");
            return;
        }

        permissions.getPlayerManager().getPlayer(player).getPermissionAttachment().unsetPermission(permission);
    }

    /**
     * Wipe the users current permissions loaded into the PermissionAttachment.
     * @param player
     */
    public void wipePermissions(Player player)
    {

        if (permissions.getPlayerManager().getPlayer(player) == null)
        {
            System.out.println(player.getName() + " has not been assigned a permission attachment!");
            return;
        }

        permissions.getPlayerManager().getPlayer(player).getPermissionAttachment().getPermissions().clear();
        System.out.println("Wiped Permissions..");
    }

    public void loadRankPermissions(Player player)
    {

        if (permissions.getPlayerManager().getPlayer(player) == null)
        {
            System.out.println(player.getName() + " has not been assigned a permission attachment!");
            return;
        }

        Rank currentRank = permissions.getRankManager().getDefaultRank();

        if (permissions.getPlayerManager().getPlayer(player).getRank() != null)
            currentRank = permissions.getPlayerManager().getPlayer(player).getRank();

        permissions.getPermissionsConfiguration().getConfig().getStringList("permissions." + currentRank.getConfigName().toUpperCase() + ".permissions").forEach(permission -> addPermission(player, permission));
    }
}
