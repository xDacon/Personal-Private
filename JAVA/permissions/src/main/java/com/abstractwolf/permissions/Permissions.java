package com.abstractwolf.permissions;

import com.abstractwolf.permissions.base.Rank;
import com.abstractwolf.permissions.command.RankCommand;
import com.abstractwolf.permissions.command.RankListCommand;
import com.abstractwolf.permissions.configuration.PermissionsConfiguration;
import com.abstractwolf.permissions.listener.CommunicationListener;
import com.abstractwolf.permissions.listener.ConnectionListener;
import com.abstractwolf.permissions.manager.PermissionManager;
import com.abstractwolf.permissions.manager.PlayerManager;
import com.abstractwolf.permissions.manager.RankManager;
import com.abstractwolf.permissions.repository.Repository;
import com.abstractwolf.permissions.util.packets.TabPacket;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class Permissions extends JavaPlugin implements CommandExecutor
{

    private Repository repository;

    private PlayerManager playerManager;
    private PermissionManager permissionManager;
    private RankManager rankManager;

    private PermissionsConfiguration permissionsConfiguration;

    @Override
    public void onEnable()
    {

        getConfig().options().copyDefaults(true);
        saveConfig();

        permissionsConfiguration = new PermissionsConfiguration(this);
        permissionsConfiguration.reloadConfig();
        permissionsConfiguration.getConfig().options().copyDefaults(true);
        permissionsConfiguration.saveConfig();

        repository = new Repository(
                getConfig().getString("settings.host"),
                getConfig().getInt("settings.port"),
                getConfig().getString("settings.database"),
                getConfig().getString("settings.username"),
                getConfig().getString("settings.password")
        );

        playerManager = new PlayerManager(this);
        permissionManager = new PermissionManager(this);
        rankManager = new RankManager();

        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                try
                {
                    PreparedStatement statement = getRepository().prepareStatement("CREATE TABLE IF NOT EXISTS data (id INT NOT NULL AUTO_INCREMENT, uuid  VARCHAR(45), rank VARCHAR(45), PRIMARY KEY (id));");
                    statement.executeUpdate();
                    statement.close();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(this);

        Bukkit.getPluginManager().registerEvents(new ConnectionListener(this), this);
        Bukkit.getPluginManager().registerEvents(new CommunicationListener(this), this);

        getCommand("rank").setExecutor(new RankCommand(this));
        getCommand("ranks").setExecutor(new RankListCommand(this));

        getPermissionsConfiguration().getConfig().getConfigurationSection("permissions").getKeys(false).forEach(key ->
        {
            rankManager.addRank(new Rank(
                    key.toUpperCase(),
                    key,
                    getPermissionsConfiguration().getConfig().getString("permissions." + key + ".prefix"),
                    getPermissionsConfiguration().getConfig().getBoolean("permissions." + key + ".default")
            ));
        });

        Bukkit.getOnlinePlayers().forEach(player ->
        {
            getPlayerManager().addPlayer(player.getUniqueId());
            getPlayerManager().getPlayer(player.getUniqueId()).loadData();

            new BukkitRunnable()
            {
                @Override
                public void run()
                {

                    if (getPlayerManager().getPlayer(player) == null)
                        return;

                    getPlayerManager().loadAttachment(player);
                    getPermissionManager().loadRankPermissions(player);
                }
            }.runTaskLater(this,  20);
        });

        new BukkitRunnable()
        {
            int i = 0;
            StringBuilder line = new StringBuilder();

            @Override
            public void run()
            {
                if (i == 3)
                    i = 0;

                Bukkit.getOnlinePlayers().forEach(player ->
                {
                    if (getPlayerManager().getPlayer(player).getRank() != null)
                    {
                        TabPacket.sendTab(player, convertColour(i) + "Welcome " + ChatColor.GRAY + "back!", ChatColor.GRAY + "Your Rank: " + getPlayerManager().getPlayer(player).getRank().getPrefix() + "\n" + convertColour(i) + convertLine(i, line));
                        player.setPlayerListName(ChatColor.translateAlternateColorCodes('&', getPlayerManager().getPlayer(player).getRank().getPrefix() + " &7" + player.getName()));
                    }
                });

                i += 1;
            }
        }.runTaskTimerAsynchronously(this, 0L, 20);
    }

    @Override
    public void onDisable()
    {
        getRepository().closeConnectionPool();
    }

    public PlayerManager getPlayerManager()
    {
        return playerManager;
    }

    public PermissionManager getPermissionManager()
    {
        return permissionManager;
    }

    public RankManager getRankManager()
    {
        return rankManager;
    }

    public PermissionsConfiguration getPermissionsConfiguration()
    {
        return permissionsConfiguration;
    }

    public Repository getRepository()
    {
        return repository;
    }

    private ChatColor convertColour(int i)
    {
        ChatColor converted = ChatColor.YELLOW;

        switch (i)
        {
            case 0:
                converted = ChatColor.LIGHT_PURPLE;
                break;
            case 1:
                converted = ChatColor.AQUA;
                break;
            case 2:
                converted = ChatColor.DARK_AQUA;
                break;
        }

        return converted;
    }

    private String convertLine(int i, StringBuilder str)
    {
        String lineTemplate = "-";

        switch (i)
        {
            case 0:
                str.append(lineTemplate);
                break;
            case 1:
                str.append(lineTemplate);
                break;
            case 2:
                str.append(lineTemplate);
                break;
        }

        if (str.toString().length() > 10)
            str.delete(1, str.length());

        return str.toString();
    }
}
