package com.abstractwolf.permissions.command;

import com.abstractwolf.permissions.Permissions;
import com.abstractwolf.permissions.base.Rank;
import com.abstractwolf.permissions.util.ChatUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class RankListCommand implements CommandExecutor
{

    public RankListCommand(Permissions permissions)
    {
        this.permissions = permissions;
    }

    private Permissions permissions;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {

        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            if (player.hasPermission("permissions.list"))
            {
                ChatUtil.sendMessage(player, "Rank", "All Ranks - &a✔ = Default &c✖ = Not Default");
            permissions.getRankManager().getRanks().forEach(rank -> ChatUtil.sendMessage(player, ChatUtil.convertUppercase(rank.getRankName()) + " &7(" + (rank.isDefaultRank() ? "&a✔" : "&c✖") + "&7)"));
        }
            else
            {
                ChatUtil.sendMessage(player, "Rank", "You do not have permission to use this command! &4permissions.list &7is required!");
            }
        }
        return false;
    }
}
