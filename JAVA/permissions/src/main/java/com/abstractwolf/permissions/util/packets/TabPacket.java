package com.abstractwolf.permissions.util.packets;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import com.abstractwolf.permissions.util.Colour;
import com.abstractwolf.permissions.util.packets.reflection.Reflection;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-27.
 */
public class TabPacket
{

	public static void sendTab(Player player, String tabTop, String tabBottom)
	{
		try
		{
			tabTop = Colour.translate(tabTop);
			tabBottom = Colour.translate(tabBottom);

			if (tabTop == null)
				tabTop = "";

			if (tabBottom == null)
				tabBottom = "";

			Object title = Reflection.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + tabTop + "\"}");
			Object subTitle = Reflection.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + tabBottom + "\"}");

			Constructor<?> titleConstructor = Reflection.getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor(Reflection.getNMSClass("IChatBaseComponent"));

			Object packet = titleConstructor.newInstance(title);
			
			try
			{
				Field field = packet.getClass().getDeclaredField("b");
				field.setAccessible(true);
				field.set(packet, subTitle);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally 
			{
				Reflection.sendPackets(player, packet);
			}
		}
		catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e)
		{
			e.printStackTrace();
		}
	}
}