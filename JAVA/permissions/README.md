# permissions system
Made this fully functioning system in less than 12 hours for a client, uses both Spigot and MySQL to store ranks and permissions.
In the permissions.yml you log a rank in a format and it allows you to set a rank to a user from that config. If the rank in the database
does not exist whilst you play you're set as the default rank which is selected from the permissions.yml.

In the future, I hope to implement inheritance so you do not have to type the same permissions for each rank along with per player permissions on each server this way you can allow a user to have 1 rank on say 10 servers but you can have different permissions for each!

Example permissions.yml:

	permissions:
  		DEFAULT:
   		 default: true
    	 prefix: ''
         permissions:
          - essentials.tp
          - essentials.warp
        ADMIN:
          default: false
          prefix: '&4Admin'
          permissions:
           - permissions.admin
           - permissions.list
           - essentials.*
        DEV:
         default: false
         prefix: '&bDev'
         permissions:
          - '*'
       OWNER:
         default: false
         prefix: '&2Owner'
         permissions:
          - '*'
