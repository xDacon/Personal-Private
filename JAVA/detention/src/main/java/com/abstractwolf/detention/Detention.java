package com.abstractwolf.detention;

import com.abstractwolf.detention.commands.DetentionCommand;
import com.abstractwolf.detention.listener.DetentionHandler;
import com.abstractwolf.detention.manager.DetentionManager;
import com.abstractwolf.detention.manager.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class Detention extends JavaPlugin
{

    private PlayerManager playerManager;
    private DetentionManager detentionManager;

    @Override
    public void onEnable()
    {

        getConfig().options().copyDefaults(true);
        saveConfig();

        Bukkit.getPluginManager().registerEvents(new DetentionHandler(this), this);
        getCommand("detention").setExecutor(new DetentionCommand(this));

        playerManager = new PlayerManager();
        detentionManager = new DetentionManager(this);

        detentionManager.startDetentionCountdown();
    }

    @Override
    public void onDisable()
    {
        detentionManager.stopDetentionCountdown();
        detentionManager.setPvpPlayer(null);
    }

    public PlayerManager getPlayerManager()
    {
        return playerManager;
    }

    public DetentionManager getDetentionManager()
    {
        return detentionManager;
    }

}
