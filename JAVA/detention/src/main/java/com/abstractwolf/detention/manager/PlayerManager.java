package com.abstractwolf.detention.manager;

import com.abstractwolf.detention.detention.DetentionedPlayer;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class PlayerManager
{

    private DetentionedPlayer detentionedPlayer;

    public void setDetentionedPlayer(Player player)
    {
        if (detentionedPlayer == null)
            System.out.println(player.getName() + " is the first player to be put in detention in this session!");

        detentionedPlayer = new DetentionedPlayer(player);
        System.out.println(player.getName() + " is the new player in detention, he was set around the time of: " + System.currentTimeMillis());
    }

    public void wipeCurrentDetention()
    {
        detentionedPlayer = null;
    }

    public DetentionedPlayer getDetentionedPlayer()
    {
        return detentionedPlayer;
    }
}
