package com.abstractwolf.detention.detention;

import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-23.
 */
public interface DetentionPlayerStats
{

    /**
     * Get a detention players UUID
     * @return UUID
     */
    UUID getUuid();

    /**
     * Get a detention players Username
     * @return String
     */
    String getUsername();

    /**
     * Get a detention players player instance.
     * @return Player
     */
    Player getPlayer();

    /**
     * Check if a player is in detention.
     * @return boolean
     */
    boolean isInDetention();

    /**
     * Set if a player is in detention.
     * @param inDetention
     */
    void setInDetention(boolean inDetention);
}
