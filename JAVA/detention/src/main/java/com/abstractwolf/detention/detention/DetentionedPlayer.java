package com.abstractwolf.detention.detention;

import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class DetentionedPlayer implements DetentionPlayerStats
{

    public DetentionedPlayer(Player player)
    {
        this.player = player;
    }

    private Player player;
    private boolean inDetention = false;

    @Override
    public UUID getUuid()
    {
        return player.getUniqueId();
    }

    @Override
    public String getUsername()
    {
        return player.getName();
    }

    @Override
    public Player getPlayer()
    {
        return player;
    }

    @Override
    public boolean isInDetention()
    {
        return inDetention;
    }

    @Override
    public void setInDetention(boolean inDetention)
    {
        this.inDetention = inDetention;
    }
}
