package com.abstractwolf.detention.commands;

import com.abstractwolf.detention.Detention;
import com.abstractwolf.detention.utils.ChatUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class DetentionCommand implements CommandExecutor
{

    public DetentionCommand(Detention plugin)
    {
        this.plugin = plugin;
    }

    private Detention plugin;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {

        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            if (!player.hasPermission("detention.admin"))
            {
                ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &cYou don't have permission for that.");
                return false;
            }

            if (plugin.getPlayerManager().getDetentionedPlayer() != null)
            {
                ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &aDetention in progress!");
                return false;
            }

            if (args.length == 1)
            {

                if (args[0].equalsIgnoreCase("start"))
                {

                    plugin.getDetentionManager().setCountdown(-1); //Technically the countdown goes to -2 for some reason so leave it at -1
                    ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &eDetention &bStarting in &e1 minute&b..");
                } else if (args[0].equalsIgnoreCase("stop"))
                {
                    if (!plugin.getDetentionManager().isRunning())
                    {
                        ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &aThe process is already stopped!");
                        return false;
                    }

                    ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &4Stopped!");
                    ChatUtil.sendBroadcastMessage("&b&lPixel&e&lBlox &8⏐ &4Detentions have been stopped!");
                    plugin.getPlayerManager().wipeCurrentDetention();
                    plugin.getDetentionManager().stopDetentionCountdown();
                }
                else if (args[0].equalsIgnoreCase("resume"))
                {
                    if (plugin.getDetentionManager().isRunning())
                    {
                        ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &aThe process is already happening!");
                        return false;
                    }

                    ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &aResumed!");
                    ChatUtil.sendBroadcastMessage("&b&lPixel&e&lBlox &8⏐ &4Detentions have been resumed!");
                    plugin.getDetentionManager().startDetentionCountdown();
                }
                else if (args[0].equalsIgnoreCase("debug"))
                {
                    plugin.getConfig().getConfigurationSection("messages").getKeys(false).forEach(key ->
                    {
                        plugin.getConfig().getStringList("messages." + key).forEach(message -> ChatUtil.sendMessage(player, message));

                        for (int i = 0; i < 2; i++)
                            ChatUtil.sendMessage(player, "");
                    });
                }
            }
            else
            {
                ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &e/detention start &f- &bSets the start time to 1 minute!");
                ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &e/detention stop &f- &bStops the detentions completely till resumed.");
                ChatUtil.sendMessage(player, "&b&lPixel&e&lBlox &8⏐ &e/detention resume &f- &bRestarts the process for detentions!");
            }
        }
        else
        {
            if (args.length == 1)
            {
                if (args[0].equalsIgnoreCase("start"))
                {
                    if (plugin.getPlayerManager().getDetentionedPlayer() != null)
                    {
                        System.out.println("Detention in progress..");
                        return false;
                    }

                    plugin.getDetentionManager().setCountdown(-1); //Technically the countdown goes to -2 for some reason so leave it at -1
                    System.out.println("Detention starting in 1 minute.");
                }
            }
            else
            {
                System.out.println("/detention start - Sets the start time to 1 minute!");
            }
        }

        return false;
    }
}
