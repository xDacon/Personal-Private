package com.abstractwolf.detention.utils;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class ChatUtil
{

    public static void sendMessage(Player player, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', message.toString()));
    }

    public static void sendBroadcastMessage(Object message)
    {
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message.toString()));
    }
}