package com.abstractwolf.detention.manager;

import com.abstractwolf.detention.Detention;
import com.abstractwolf.detention.detention.DetentionedPlayer;
import com.abstractwolf.detention.utils.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class DetentionManager
{

    public DetentionManager(Detention plugin)
    {
        this.plugin = plugin;
    }

    private Detention plugin;

    private Player pvpPlayer;

    private boolean isRunning = false;

    private BukkitTask countdownRunnable;
    private int defaultCountdown = 30, countdown = defaultCountdown;

    public void startDetentionCountdown()
    {

        countdownRunnable = new BukkitRunnable()
        {
            public void run()
            {

                if (countdown == 31)
                {
                    if (plugin.getPlayerManager().getDetentionedPlayer() != null)
                    {
                        DetentionedPlayer player = plugin.getPlayerManager().getDetentionedPlayer();

                        if (player.isInDetention())
                        {
                            plugin.getConfig().getStringList("messages.ended_message_detention").forEach(message -> ChatUtil.sendBroadcastMessage(message.replace("<chosen>", player.getUsername())));
                            plugin.getConfig().getStringList("commands.end_rewards").forEach(command -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("<player>", player.getUsername())));
                        }
                        else
                        {
                            pvpPlayer = player.getPlayer();
                            plugin.getConfig().getStringList("messages.ended_message_pvp").forEach(message -> ChatUtil.sendBroadcastMessage(message.replace("<chosen>", player.getUsername())));
                            plugin.getConfig().getStringList("commands.end_evil_rewards").forEach(command -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("<player>", player.getUsername())));
                        }
                        plugin.getPlayerManager().wipeCurrentDetention();
                    }
                }

                if (countdown < 0)
                {
                    if (Bukkit.getOnlinePlayers().size() < 1)
                    {
                        countdown = 32;
                        return;
                    }

                    int random = new Random().nextInt(Bukkit.getOnlinePlayers().size());
                    int onlineAmount = 0;

                    for (Player player : Bukkit.getOnlinePlayers())
                    {
                        if (onlineAmount == random)
                        {
                            plugin.getConfig().getStringList("messages.started_message_player").forEach(message -> ChatUtil.sendMessage(player, message.replace("<chosen>", player.getName())));
                            plugin.getConfig().getStringList("messages.started_message").forEach(message -> ChatUtil.sendBroadcastMessage(message.replace("<chosen>", player.getName())));
                            plugin.getPlayerManager().setDetentionedPlayer(player);
                            player.getWorld().playSound(player.getLocation(),Sound.ENDERDRAGON_DEATH,1, 0);
                            countdown = 32;
                        }

                        onlineAmount += 1;
                    }
                    return;
                }

                countdown -= 1;
                System.out.println("Countdown in minutes: " + countdown);
            }//
        }.runTaskTimerAsynchronously(plugin, 0L, 60 * 20); //Called every minute 60 * 20

        isRunning = true;
    }

    public void stopDetentionCountdown()
    {
        if (countdownRunnable != null)
        {
            countdownRunnable.cancel();
            countdownRunnable = null;
            System.out.println("Cancelled the detention task!");
        }

        isRunning = false;
    }

    public boolean isRunning()
    {
        return isRunning;
    }

    public void setRunning(boolean running)
    {
        isRunning = running;
    }

    public int getDefaultCountdown()
    {
        return defaultCountdown;
    }

    public int getCountdown()
    {
        return countdown;
    }

    public void setCountdown(int countdown)
    {
        this.countdown = countdown;
    }

    public void setPvpPlayer(Player pvpPlayer)
    {
        this.pvpPlayer = pvpPlayer;
    }

    public Player getPvpPlayer()
    {
        return pvpPlayer;
    }
}
