package com.abstractwolf.detention.listener;

import com.abstractwolf.detention.Detention;
import com.abstractwolf.detention.utils.ChatUtil;
import com.abstractwolf.detention.utils.LocationUtil;
import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.mewin.WGRegionEvents.events.RegionLeaveEvent;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

/**
 * Created by ThatAbstractWolf on 2016-12-21.
 */
public class DetentionHandler implements Listener
{

    public DetentionHandler(Detention detention)
    {
        this.detention = detention;
    }

    private Detention detention;

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {

        if (detention.getDetentionManager().getPvpPlayer() != null && detention.getDetentionManager().getPvpPlayer().equals(event.getPlayer()))
        {
            ApplicableRegionSet regions = getWorldGuard().getRegionManager(event.getPlayer().getWorld()).getApplicableRegions(event.getPlayer().getLocation());

            regions.getRegions().forEach(region ->
            {
                if (region.getId().equalsIgnoreCase("pvp"))
                    event.getPlayer().damage(20);
            });
        }
        else
        {
            if (detention.getPlayerManager().getDetentionedPlayer() != null && detention.getPlayerManager().getDetentionedPlayer().getPlayer().equals(event.getPlayer()))
            {
                event.getPlayer().damage(20);
                detention.getPlayerManager().wipeCurrentDetention();
            }
        }

        if (detention.getDetentionManager().getPvpPlayer() == null || !detention.getDetentionManager().getPvpPlayer().equals(event.getPlayer()))
            return;

        detention.getDetentionManager().setPvpPlayer(null);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event)
    {
        if (detention.getDetentionManager().getPvpPlayer() == null || !detention.getDetentionManager().getPvpPlayer().equals(event.getEntity()))
            return;

        detention.getDetentionManager().setPvpPlayer(null);
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event)
    {

        if (detention.getDetentionManager().getPvpPlayer() == null || !detention.getDetentionManager().getPvpPlayer().equals(event.getPlayer()))
            return;

        ApplicableRegionSet regions = getWorldGuard().getRegionManager(event.getPlayer().getWorld()).getApplicableRegions(event.getPlayer().getLocation());

        regions.getRegions().forEach(region ->
        {
            if (region.getId().equalsIgnoreCase("pvp"))
            {
                if (event.getMessage().contains("/"))
                    event.setCancelled(true);
            }
        });
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent event)
    {
        if (event.getRegion().getId().equals(detention.getConfig().getString("detention.roomregion")))
        {
            if (detention.getPlayerManager().getDetentionedPlayer() != null && detention.getPlayerManager().getDetentionedPlayer().getPlayer().equals(event.getPlayer()) && !detention.getPlayerManager().getDetentionedPlayer().isInDetention())
            {
                detention.getConfig().getStringList("messages.enterregion").forEach(message -> ChatUtil.sendMessage(event.getPlayer(), message.replace("<player>", event.getPlayer().getName())));
                detention.getPlayerManager().getDetentionedPlayer().setInDetention(true);
            }
        }
    }

    @EventHandler
    public void onRegionExit(RegionLeaveEvent event)
    {

        if (event.getRegion().getId().equals(detention.getConfig().getString("detention.roomregion")))
        {
            if (detention.getPlayerManager().getDetentionedPlayer() != null && detention.getPlayerManager().getDetentionedPlayer().getPlayer().equals(event.getPlayer()) && detention.getPlayerManager().getDetentionedPlayer().isInDetention())
            {
                detention.getConfig().getStringList("messages.exitregion").forEach(message -> ChatUtil.sendMessage(event.getPlayer(), message.replace("<player>", event.getPlayer().getName())));
                event.getPlayer().teleport(LocationUtil.parseStringToLocation(detention.getConfig().getString("teleport.roomlocation")));
            }
        }
    }

    public WorldGuardPlugin getWorldGuard()
    {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

        if (plugin == null || !(plugin instanceof WorldGuardPlugin))
        {
            System.out.println("Cannot find worldguard!");
            return null;
        }
        return (WorldGuardPlugin) plugin;
    }
}
