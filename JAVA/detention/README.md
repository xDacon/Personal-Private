# detention - pixelblox
This is a small system for a server named Pixelblox, the concept is for a prison server and every 30 minutes a random player gets chosen to go to detention.
If the player goes into a detention set region with the WorldguardAPI they get rewarded and they must stay there for 1 minute. If they do not enter the region,
they get no reward and get teleported into a pvp arena where they cannot escape without dying. I guess the concept was to keep players in suspense. 