# Quest Framework
This is a Framework for creating quests for your players.. to create a new Quest it is fairly simple. You only need todo something similar to:

  public class MineplexApplicationQuest extends Quest
  {

	public MineplexApplicationQuest()
	{
		super("Mineplex App Quest", new Location(Bukkit.getWorld("world"), -10, 41, 0, -179, 2), new String[] 
				{
						"Welcome to the quest to find Williams book!",
						"To find out what you need todo next locate William!",
						"He will tell you what is wrong.. His Location:-4, 41, -12",
						"Once you collect it return it to Brutus to give back to me!",
						"",
						"This was made to display to Mineplex and anyone relating to Mineplex.."
				},
				new QuestArmourPart(new SkullBuilder().setOwner("Chiss").build(), null, null, null, null));

		addPart(new QuestPart(this, "WilliamTiger", 1, new Location(Bukkit.getWorld("world"), -4, 41, -12), new String[] 
				{
						"Hello friend! I have been looking for someone for hours!",
						"My friend, Adam, has my Java book!",
						"Can you help me find him? He was last seen",
						"Around the coords: -17, 41, -2! Thank you!"
				}, null, null,
				new QuestArmourPart(new SkullBuilder().setOwner("WilliamTiger").build(), null, null, null, null)));

		addPart(new QuestPart(this, "Adam (582)", 2, new Location(Bukkit.getWorld("world"), -17, 41, -2, -90, 6), new String[] 
				{
						"Hello there traveller! What brings you here?",
						"Oh wait.. let me guess? William wants his Java book back haha.",
						"Sure.. Let me just find it, oh wait it is with Alex.",
						"He is around the coords, 2, 41, -8!"
				}, null, null,
				new QuestArmourPart(new SkullBuilder().setOwner("582").build(), null, null, null, null)));

		addPart(new QuestPart(this, "AlexTheCoder", 3, new Location(Bukkit.getWorld("world"), 2, 41, -8, 91, 0), new String[] 
				{
						"Oh you're looking for Williams Java book?",
						"Sure let me just find it.. Here it is.",
						"William wants it returned to Brutus at: 13, 42, 2",
				}, new ItemStackBuilder(Material.BOOK).setName(Colour.translate("&aJava Book")).build(), null,
				new QuestArmourPart(new SkullBuilder().setOwner("AlexTheCoder").build(), null, null, null, null)));

		addPart(new QuestPart(this, "Brutus (Brvtvs)", 4, new Location(Bukkit.getWorld("world"), 13, 42, 2, 90, 2), new String[] 
				{
						"Oh thank you! William will be most greatful!",
						"Here take 10 diamonds for your troubles!",
				}, new ItemStack(Material.DIAMOND, 10), new ItemStack(Material.BOOK), 
				new QuestArmourPart(new SkullBuilder().setOwner("Brvtvs").build(), null, null, null, null)));
	}
  }
