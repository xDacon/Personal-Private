package com.abstractwolf.quests.framework;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.abstractwolf.quests.framework.questparts.QuestPart;
import com.abstractwolf.quests.utils.Colour;
import com.abstractwolf.quests.utils.UtilChat;

public class QuestHandler implements Listener
{

	private QuestManager manager;

	public QuestHandler(JavaPlugin plugin, QuestManager manager)
	{
		this.manager = manager;

		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onQuestInteract(PlayerInteractAtEntityEvent event)
	{

		Player player = event.getPlayer();

		if (event.getRightClicked() instanceof ArmorStand)
		{
			event.setCancelled(true);

			Entity entity = (ArmorStand) event.getRightClicked();

			if (!manager.getSelectedQuest().containsKey(player))
			{
				for (Quest quests : manager.getQuests())
				{

					if (entity.getCustomName().equalsIgnoreCase(Colour.translate("&b&l" + quests.getQuestName())))
					{
						UtilChat.message(player, "Quest", "&7Started &b" + quests.getQuestName() + "&7!");

						manager.setPlayersQuest(player, quests);

						for (int i = 0; i < 10; i++)
							UtilChat.message(player, "");

						quests.sendIntroductionMessage(player);
					}
				}
			}
			else
			{

				for (Quest quests : manager.getQuests())
				{

					if (manager.getSelectedQuest().get(player) != quests && entity.getCustomName().equalsIgnoreCase(Colour.translate("&b&l" + quests.getQuestName())))
					{
						UtilChat.message(player, "Quest", "&7You're already in a Quest! &b" + manager.getSelectedQuest().get(player).getQuestName());
						return;
					}
				}

				for (QuestPart parts : manager.getSelectedQuest().get(player).getQuestParts())
				{
					if (entity.getCustomName().contains(ChatColor.stripColor(manager.getSelectedQuest().get(player).getQuestName())) && entity.getCustomName().contains(ChatColor.stripColor(parts.getPartName())))
					{

						if (manager.getSelectedQuestPart().get(player) == null && parts.getPartID() != 1)
						{
							UtilChat.message(player, "Quest", "&7You need to get part&f:&a 1 &7first!");
							return;
						}

						if (manager.getSelectedQuestPart().get(player) != null && parts.getPartID() != (manager.getSelectedQuestPart().get(player).getPartID() + 1))
						{
							UtilChat.message(player, "Quest", "&7You need to get a different part first! &7Part&f: &a" + (manager.getSelectedQuestPart().get(player).getPartID() + 1));
							return;
						}

						if (manager.getSelectedQuestPart().get(player) != null && parts.getRequires() != null)
						{

							if (!player.getItemInHand().getType().equals(parts.getRequires().getType()))
							{
								UtilChat.message(player, manager.getSelectedQuest().get(player).getQuestName(), "You need &a" + parts.getRequires().getType() + " &7to continue!");
								return;
							}

							player.getInventory().remove(player.getInventory().getItemInHand());
						}

						try
						{
							if (manager.getSelectedQuestPart().get(player) != null && manager.getSelectedQuest().get(player).getQuestParts().get(manager.getSelectedQuestPart().get(player).getPartID() + 1) == null)
								System.out.println("Part: " + manager.getSelectedQuestPart().get(player).getPartID());


						}
						catch (IndexOutOfBoundsException e)
						{
							for (int i = 0; i < 10; i++)
								UtilChat.message(player, "");
							
							manager.setQuestPart(player, parts);
							
							manager.getSelectedQuestPart().get(player).addItems(player, manager.getSelectedQuestPart().get(player).getAddItem());
							manager.getSelectedQuestPart().get(player).sendPartMessage(player);
							
							UtilChat.message(player, "");
							UtilChat.message(player, "Quest", "&aCongratulations&7! You completed the quest&f: &a" + manager.getSelectedQuest().get(player).getQuestName() +  "&7!");

							manager.getSelectedQuest().remove(player);
							manager.getSelectedQuestPart().remove(player);
							return;
						}

						manager.setQuestPart(player, parts);

						for (int i = 0; i < 10; i++)
							UtilChat.message(player, "");

						manager.getSelectedQuestPart().get(player).addItems(player, manager.getSelectedQuestPart().get(player).getAddItem());
						manager.getSelectedQuestPart().get(player).sendPartMessage(player);
					}
				}
			}

			return;
		}
	}

	@EventHandler
	public void onBreak(EntityDamageByEntityEvent event)
	{

		for (Quest quests : manager.getQuests())
		{
			if (event.getEntity() instanceof ArmorStand && event.getEntity().getCustomName().contains(quests.getQuestName()))
			{
				event.setCancelled(true);
			}
		}
	}
}
