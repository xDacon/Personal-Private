package com.abstractwolf.quests.types;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.abstractwolf.quests.framework.Quest;
import com.abstractwolf.quests.framework.questparts.QuestPart;
import com.abstractwolf.quests.utils.UtilInventory;

public class Quest1 extends Quest
{

	public Quest1()
	{
		super("Quest of the lost book", new Location(Bukkit.getWorld("world"), 0, 55, 0), new String[] 
				{
						"Welcome to the quest to find the lost book",
						"To find out what you need todo next locate Jack!",
						"He will tell you what is wrong.. His Location: 0, 75, 0"
				});

		/*
		 * Please always start from 1! So 1, 2, 3 not 0, 1, 2, 3 or 0, 2, 1, 3 always 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 etc..
		 * 
		 * WARNING:
		 * If you disgard this the plugin may break and will not work as we check if they have not found 1 yet if it equals 1!
		 */

		addPart(new QuestPart(this, "Jack", 1, new Location(Bukkit.getWorld("world"), 0, 75, 0), new String[] 
				{
						"Hello! Than god you're here!!!",
						"My friend, John, has my book!",
						"Can you help me find him? He was last seen",
						"Around the coords: 0, 85, 0! Thank you!"
				}, null));

		addPart(new QuestPart(this, "John", 2, new Location(Bukkit.getWorld("world"), 0, 85, 0), new String[] 
				{
						"Hello there traveller! What brings you here?",
						"Oh wait.. let me guess? Jack wants his book back haha.",
						"Sure.. Let me just find it, oh here it is.",
						"Tell him I said thank you."
				}, null) 
		{
			@Override
			public void addItems(Player player)
			{
				player.getInventory().addItem(new ItemStack(Material.BOOK, 1));
			}
		});

		addPart(new QuestPart(this, "Jack", 3, new Location(Bukkit.getWorld("world"), 0, 65, 0), new String[] 
				{
						"Oh you have it? Thank you so much!",
						"For your help take 10 diamonds! It is",
						"The least I can do for you, such a king man!",
				}, null)
		{

			@Override
			public void addItems(Player player)
			{
				player.getInventory().addItem(new ItemStack(Material.DIAMOND_AXE, 10));
			}
			 
			@Override
			public void removeItem(Player player)
			{
				
				//TODO
				
				if (UtilInventory.hasItem(player, Material.BOOK))
					System.out.println("Yes");
				else
					System.out.println("No");
				
				if (UtilInventory.hasItem(player, Material.BOOK))
					UtilInventory.removeItem(player, Material.BOOK);
			}
		});
	}
}
