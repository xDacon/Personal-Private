package com.abstractwolf.quests.utils;

import org.bukkit.entity.Player;

public class UtilChat
{

	public static void createLine(Player player) 
	{ 
		String defaultLine = "■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■";
		
		StringBuilder line = new StringBuilder();
		
		for (int i = 0; i < defaultLine.length() / 2; i++)
		{
			if (i % 2 == 0)
			{
				line.append(Colour.translate("&7■")).append(" ");
			}
			else
			{
				line.append(Colour.translate("&8■")).append(" ");
			}
		}
		player.sendMessage(line.toString()); 
	}

	public static void message(Player player, String message) { player.sendMessage(Colour.translate("&7" + message)); }

	public static void message(Player player, String prefix, String message) { player.sendMessage(Colour.translate("&3&l" + prefix + " &8>> &7" + message)); }
}
