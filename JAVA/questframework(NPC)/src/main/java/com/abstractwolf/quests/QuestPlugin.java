package com.abstractwolf.quests;

import org.bukkit.plugin.java.JavaPlugin;

import com.abstractwolf.quests.framework.QuestHandler;
import com.abstractwolf.quests.framework.QuestManager;
import com.abstractwolf.quests.types.MineplexApplicationQuest;

public class QuestPlugin extends JavaPlugin
{
	
	private QuestManager manager;

	@Override
	public void onEnable()
	{
		
		manager = new QuestManager();
		
		addQuests();
		manager.spawnQuests();
		
		new QuestHandler(this, manager);
		
		System.out.println("Quest Framework Enabled!");
	}
	
	@Override
	public void onDisable()
	{
		
		manager.clearQuests();
		System.out.println("Quest Framework Disabled!");
	}
	
	public void addQuests()
	{
		getQuestManager().addQuest(new MineplexApplicationQuest());
	}
	
	public QuestManager getQuestManager()
	{
		return manager;
	}
}
