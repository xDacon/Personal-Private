package com.abstractwolf.quests.framework.questparts;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.abstractwolf.quests.framework.Quest;
import com.abstractwolf.quests.utils.Colour;
import com.abstractwolf.quests.utils.UtilChat;

public class QuestPart
{

	private ArmorStand stand;

	private Quest originalQuest;
	private QuestArmourPart armourPart;

	private String partName;
	private int partID;
	private String[] partMessage;
	private Location partLocation;

	private ItemStack requires, addItem;

	private List<ArmorStand> stands = new ArrayList<>();

	public QuestPart(Quest originalQuest, String partName, int partID, Location partLocation, String[] partMessage, ItemStack addItem, ItemStack requires, QuestArmourPart armourpart)
	{

		this.originalQuest = originalQuest;
		this.partID = partID;
		this.partName = partName;
		this.partLocation = partLocation;
		this.partMessage = partMessage;

		this.armourPart = armourpart;
		
		this.addItem = addItem;
		this.requires = requires;
	}

	public void spawnPartNPC()
	{

		stand = partLocation.getWorld().spawn(partLocation, ArmorStand.class);

		stand.setCustomName(Colour.translate("&a&l" + originalQuest.getQuestName() + " &8>> &7" + partName + " &8>> &7" + partID));
		stand.setCustomNameVisible(true);

		stand.setVisible(true);
		stand.setGravity(false);
		stand.setRemoveWhenFarAway(false);

		if (armourPart != null)
			armourPart.applyArmour(stand);

		stands.add(stand);

	}

	public void addItems(Player player, ItemStack item) 
	{

		if (item == null)
			return;

		player.getInventory().addItem(item);	

		UtilChat.message(player, getOriginalQuest().getQuestName(), "Added &a" + item.getType() + " &7to your inventory!");
	}

	public void sendPartMessage(Player player)
	{

		UtilChat.createLine(player);
		UtilChat.message(player, "&f&l" + partName);

		for (String message : partMessage)
			UtilChat.message(player, message);
		UtilChat.createLine(player);
	}

	public ArmorStand getStand()
	{
		return stand;
	}

	public Quest getOriginalQuest()
	{
		return originalQuest;
	}

	public String getPartName()
	{
		return partName;
	}

	public int getPartID()
	{
		return partID;
	}

	public Location getPartLocation()
	{
		return partLocation;
	}

	public String[] getPartMessage()
	{
		return partMessage;
	}

	public List<ArmorStand> getStands()
	{
		return stands;
	}
	
	public ItemStack getAddItem()
	{
		return addItem;
	}

	public ItemStack getRequires()
	{
		return requires;
	}

	public void clearStands()
	{
		if (stands.isEmpty())
			return;

		stands.clear();
	}
}
