package com.abstractwolf.quests.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class UtilInventory
{

	public static boolean hasItem(Player player, Material material)
	{
		
		for (ItemStack items : player.getInventory().getContents())
		{
			if (items.getType().equals(material))
				return true;
			else
				return false;
		}
		
		return false;
	}
	
	public static void removeItem(Player player, Material material)
	{
		for (ItemStack items : player.getInventory().getContents())
		{
			if (items.getType().equals(material))
				items.setType(Material.AIR);
			
			System.out.println(items.getType());
		}
	}
}
