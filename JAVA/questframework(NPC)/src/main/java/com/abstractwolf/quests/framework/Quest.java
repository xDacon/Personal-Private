package com.abstractwolf.quests.framework;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;

import com.abstractwolf.quests.framework.questparts.QuestArmourPart;
import com.abstractwolf.quests.framework.questparts.QuestPart;
import com.abstractwolf.quests.utils.Colour;
import com.abstractwolf.quests.utils.UtilChat;

public abstract class Quest
{
	
	private String questName;
	private Location questBeginning;
	private String[] introductionMessage;
	
	private QuestArmourPart armourPart;
	
	private ArrayList<QuestPart> questParts;
	
	public Quest(String questName, Location questBeginning, String[] introductionMessage, QuestArmourPart armourPart)
	{
		
		questParts = new ArrayList<>();
		
		this.questName = questName;
		this.questBeginning = questBeginning;
		this.introductionMessage = introductionMessage;
		
		this.armourPart = armourPart;
	}
	
	public void buildQuest()
	{
		
		/* Spawns QuestNPC not parts.. */
		spawnBeginningNPC();
		
		/* Spawns each quest part NPCs.. */
		for (QuestPart parts : questParts)
			parts.spawnPartNPC();
		
	}
	
	private void spawnBeginningNPC()
	{
		
		ArmorStand stand = questBeginning.getWorld().spawn(questBeginning, ArmorStand.class);
		
		stand.setCustomName(Colour.translate("&b&l" + questName));
		stand.setCustomNameVisible(true);
		
		stand.setVisible(true);
		stand.setGravity(false);
		stand.setRemoveWhenFarAway(false);
		
		if (armourPart != null)
			armourPart.applyArmour(stand);
	}
	
	public void sendIntroductionMessage(Player player)
	{

		UtilChat.createLine(player);
		for (String message : introductionMessage)
			UtilChat.message(player, message);
		UtilChat.createLine(player);
	}
	
	public void addPart(QuestPart part)
	{
		if (questParts.contains(part))
			return;
		
		questParts.add(part);
	}
	
	public void removePart(QuestPart part)
	{
		if (!questParts.contains(part))
			return;
	
		questParts.remove(part);
	}
	
	public String getQuestName()
	{
		return questName;
	}
	
	public Location getQuestBeginning()
	{
		return questBeginning;
	}
	
	public QuestArmourPart getArmourPart()
	{
		return armourPart;
	}
	
	public ArrayList<QuestPart> getQuestParts()
	{
		return questParts;
	}
}
