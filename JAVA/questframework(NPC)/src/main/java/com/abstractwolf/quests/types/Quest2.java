package com.abstractwolf.quests.types;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.abstractwolf.quests.framework.Quest;
import com.abstractwolf.quests.framework.questparts.QuestArmourPart;
import com.abstractwolf.quests.framework.questparts.QuestPart;

public class Quest2 extends Quest
{

	public Quest2()
	{
		super("Quest 2 Find bob!", new Location(Bukkit.getWorld("world"), 0, 55, 0), new String[] 
				{
						"Welcome to Quest 2, this is used to teach you how to use Quests!",
						"To begin with locate Part 1 which is located at: world, 0, 75, 0!",
						"Goodluck!"
				});

	}

	@Override
	public void buildQuestParts()
	{
		
		/*
		 * Please always start from 1! So 1, 2, 3 not 0, 1, 2, 3 or 0, 2, 1, 3 always 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 etc..
		 * 
		 * WARNING:
		 * If you disgard this the plugin may break and will not work as we check if they have not found 1 yet if it equals 1!
		 */

		addPart(new QuestPart(this, "Part 1", 1, new Location(Bukkit.getWorld("world"), 0, 75, 0), new String[] 
				{
						"Welcome to Part 1 of this Default Quesat",
						"This is mainly a test to try out messages"
				}, null));

		addPart(new QuestPart(this, "Part 2", 2, new Location(Bukkit.getWorld("world"), 0, 90, 0), 
				new String[] 
						{
								"Welcome to Part 2 of this Default Quesat",
								"This is mainly a test to try out messages"
						}, new QuestArmourPart(new ItemStack[] 
								{
										new ItemStack(Material.IRON_HELMET), 
										new ItemStack(Material.GOLD_CHESTPLATE),
										new ItemStack(Material.DIAMOND_LEGGINGS),
										null
								})));
		
		addPart(new QuestPart(this, "Part 3", 3, new Location(Bukkit.getWorld("world"), 0, 110, 0), 
				new String[] 
						{
								"Welcome to Part 3 of this Default Quesat",
								"This is mainly a test to try out messages"
						}, new QuestArmourPart(new ItemStack[] 
								{
										new ItemStack(Material.IRON_HELMET), 
										new ItemStack(Material.GOLD_CHESTPLATE),
										null,
										null
								})));

		System.out.println("Built parts for " + getQuestName());
	}
}
