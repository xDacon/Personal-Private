package com.abstractwolf.quests.framework.questparts;

import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

public class QuestArmourPart
{

	private ItemStack helmet, chestplate, leggings, boots, hand;

	public QuestArmourPart(ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots, ItemStack hand)
	{	
		this.helmet = helmet;
		this.chestplate = chestplate;
		this.leggings = leggings;
		this.boots = boots;
		this.hand = hand;
	}

	public void applyArmour(ArmorStand armourStand)
	{

		if (armourStand == null)
			return;

		if (helmet != null)
			armourStand.setHelmet(helmet);
		if (chestplate != null)
			armourStand.setChestplate(chestplate);
		if (leggings != null)
			armourStand.setLeggings(leggings);
		if (boots != null)
			armourStand.setBoots(boots);
		if (hand != null)
			armourStand.setItemInHand(hand);

		System.out.println("Armour Applied!");
	}
}
