package com.abstractwolf.quests.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.abstractwolf.quests.framework.questparts.QuestPart;

public class QuestManager
{

	private HashMap<Player, Quest> selectedQuest = new HashMap<>();
	private HashMap<Player, QuestPart> selectedQuestPart = new HashMap<>();
	
	private List<Quest> quests = new ArrayList<>();
	
	public void setPlayersQuest(Player player, Quest quest)
	{
		selectedQuest.put(player, quest);
		System.out.println("Set: " + player.getName() + "'s Quest to: " + quest.getQuestName());
	}
	
	public void setQuestPart(Player player, QuestPart part)
	{
		
		if (selectedQuest.get(player) != null && selectedQuest.get(player).getQuestParts().contains(part))
		{
			selectedQuestPart.put(player, part);
			System.out.println("Set: " + player.getName() + "'s Quest Part to: " + part.getPartName());
		}
	}
	
	public void spawnQuests()
	{
		for (Quest quest : quests)
		{
			quest.buildQuest();
		}
	}
	
	public void clearQuests()
	{
		
		for (Quest quest : quests)
		{
			System.out.println("Removing: " + quest.getQuestName());
			
			for (Entity entity : quest.getQuestBeginning().getWorld().getEntities())
			{
				
				if (entity instanceof ArmorStand && entity.getCustomName().contains(quest.getQuestName()))
					entity.remove();
			}
			
			for (QuestPart questParts : quest.getQuestParts())
			{
				
				for (int i = 0; i < questParts.getStands().size(); i++)
				{
					questParts.getStands().get(i).remove();
					questParts.getStands().remove(i);

				}
			}
		}
	}
	
	public List<Quest> getQuests() { return quests; }
	
	public HashMap<Player, Quest> getSelectedQuest() { return selectedQuest; }
	public HashMap<Player, QuestPart> getSelectedQuestPart() { return selectedQuestPart; }
	
	public <T extends Quest> void addQuest(T quest)
	{
		if (quests.contains(quest))
			return;
		
		quests.add(quest);
	}
}
