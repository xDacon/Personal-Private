package com.abstractstudios.engine.game.team;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;

/**
 * Created by ThatAbstractWolf on 2016-12-23.
 */
public enum TeamData
{

    /*
     * I will add more shortly, just for now we only need 4.
     */
    GREEN(ChatColor.GREEN, DyeColor.GREEN, new Location(Bukkit.getWorld("world"), 220, 64, 114, 2, 12)),
    RED(ChatColor.DARK_RED, DyeColor.RED, new Location(Bukkit.getWorld("world"), 221, 64, 114, 2, 12)),
    BLUE(ChatColor.DARK_BLUE, DyeColor.BLUE, new Location(Bukkit.getWorld("world"), 222, 64, 114, 2, 12)),
    YELLOW(ChatColor.YELLOW, DyeColor.YELLOW, new Location(Bukkit.getWorld("world"), 223, 64, 114, 2, 12));

    TeamData(ChatColor colourEquivalent, DyeColor colourData, Location spawnLocation)
    {
        this.colourEquivalent = colourEquivalent;
        this.colourData = colourData;
        this.spawnLocation = spawnLocation;
    }

    private ChatColor colourEquivalent;
    private DyeColor colourData;
    private Location spawnLocation;

    /**
     * Team Colours chat colour equivalent.
     * @return ChatColor
     */
    public ChatColor getColourEquivalent()
    {
        return colourEquivalent;
    }

    /**
     * Team Colours but in data form for sheep!
     * @return DyeColor
     */
    public DyeColor getColourData()
    {
        return colourData;
    }

    /**
     * Get the spawn location for a sheep (podium at spawn).
     * @return Location
     */
    public Location getSpawnLocation()
    {
        return spawnLocation;
    }
}
