package com.abstractstudios.engine.game.type;

import com.abstractstudios.engine.game.TeamGame;
import com.abstractstudios.engine.game.team.Team;
import com.abstractstudios.engine.game.team.TeamData;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class TeamSpleef extends TeamGame
{

    public TeamSpleef()
    {
        super("Team Spleef", new String[]
                {
                        "Temp",
                        "Temp",
                        "Temp"
                }, 4);

        addTeam(new Team("Blue", TeamData.BLUE));
        addTeam(new Team("Red", TeamData.RED));
    }
}
