package com.abstractstudios.engine.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 *
 * @author Camouflage100
 * @author ThatAbstractWolf
 */
public class UtilChat
{

    /**
     * Send a player to a message.
     * @param player
     * @param message
     */
    public static void sendMessage(Player player, Object message)
    {
        player.sendMessage(translateColour("&7" + message));
    }

    /**
     * Send a player to a message with a prefix.
     * @param player
     * @param prefix
     * @param message
     */
    public static void sendMessage(Player player, Object prefix, Object message)
    {
        player.sendMessage(translateColour(prefix + " &8>> &7" + message));
    }

    /**
     * Send a broadcast to all online players.
     * @param message
     */
    public static void sendBroadcast(Object message)
    {
        Bukkit.broadcastMessage(translateColour("&7" + message));
    }

    /**
     * Send a broadcast to all online players with a prefix.
     * @param prefix
     * @param message
     */
    public static void sendBroadcast(Object prefix, Object message) { Bukkit.broadcastMessage(translateColour(prefix + " &8>> &7" + message)); }

    /**
     * Translate colour codes. (Made by Camouflage100)
     * @param message
     * @return
     */
    public static String translateColour(String message) { return ChatColor.translateAlternateColorCodes('&', message); }
}
