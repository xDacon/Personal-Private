package com.abstractstudios.engine.game.type;

import com.abstractstudios.engine.game.Game;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class Spleef extends Game
{

    public Spleef()
    {
        super("Spleef", new String[]
                {
                        "Temp",
                        "Temp",
                        "Temp"
                }, 3);
    }
}
