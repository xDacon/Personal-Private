package com.abstractstudios.engine.game.team.gui;

import com.abstractstudios.engine.GameEngine;
import com.abstractstudios.engine.game.TeamGame;
import com.abstractstudios.engine.game.team.Team;
import com.abstractstudios.engine.utils.UtilItemBuilder;
import com.abstractstudios.engine.utils.menu.Menu;
import com.abstractstudios.engine.utils.menu.MenuItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

/**
 * Created by ThatAbstractWolf on 2016-12-23.
 */
public class TeamGUI extends Menu
{

    public TeamGUI(GameEngine gameEngine, Player player)
    {
        super(gameEngine, "Team Selector", 5);

        int slot = 9;

        addItem(new MenuItem(4, new UtilItemBuilder(Material.WOOL).build()));

        for (Team teams : ((TeamGame) gameEngine.getGameManager().getCurrentGame()).getGameTeams())
        {
            addItem(new MenuItem(slot,
                    new UtilItemBuilder(Material.WOOL, teams.getTeamColour().getColourData().getWoolData())
                            .setName(teams.getTeamColour().getColourEquivalent() + "TEAM")
                            .setLore(ChatColor.GRAY + "This team is assigned for the gamemode: " + gameEngine.getGameManager().getCurrentGame().getGameName())
                            .build())
            {

                @Override
                public void click(Player player, ClickType clickType)
                {
                    //TODO
                }
            });

            slot += 1;
        }

        openInventory(player);
    }
}
