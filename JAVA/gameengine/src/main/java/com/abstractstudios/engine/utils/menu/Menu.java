package com.abstractstudios.engine.utils.menu;

import java.util.ArrayList;
import java.util.List;

import com.abstractstudios.engine.GameEngine;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class Menu implements InventoryHolder
{

	public Menu(GameEngine gameEngine, String title, int rows)
	{
		this.gameEngine = gameEngine;
		this.title = title;
		this.rows = rows;

		items = new ArrayList<>();
		inventory = Bukkit.createInventory(this, (rows * 9), ChatColor.translateAlternateColorCodes('&', title));
	}

	protected GameEngine gameEngine;

	private final Inventory inventory;

	private final String title;
	private final int rows;

	private List<MenuItem> items;

	public void addItem(MenuItem item)
	{
		items.add(item);
	}

	public void openInventory(Player player)
	{
		if (inventory == null)
			return;

		inventory.clear();

		for (MenuItem item : items)
			inventory.setItem(item.getIndex(), item.getItemStack());

		player.openInventory(inventory);
	}

	@Override
	public Inventory getInventory()
	{
		return inventory;
	}

	public String getTitle()
	{
		return title;
	}

	public int getRows()
	{
		return rows;
	}

	public List<MenuItem> getItems()
	{
		return items;
	}
}
