package com.abstractstudios.engine.game.team;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-23.
 */
public class Team
{

    public Team(String teamName, TeamData teamColour)
    {
        this.teamName = teamName;
        this.teamColour = teamColour;
        this.teamMembers = new ArrayList<>();
    }

    private String teamName;
    private TeamData teamColour;

    private List<UUID> teamMembers;

    /**
     * Get the Team name.
     * @return String
     */
    public String getTeamName()
    {
        return teamName;
    }

    /**
     * Get the Teams colour
     * @return TeamColour
     */
    public TeamData getTeamColour()
    {
        return teamColour;
    }

    /**
     * Add a new member to a team.
     * @param player
     */
    public void addTeamMember(Player player)
    {
        if (teamMembers.contains(player.getUniqueId()))
        {
            System.out.println(teamName + " already contains " + player.getName() + " as a member!");
            return;
        }

        teamMembers.add(player.getUniqueId());
    }

    /**
     * Remove a member from a team.
     * @param player
     */
    public void removeTeamMember(Player player)
    {
        if (!teamMembers.contains(player.getUniqueId()))
        {
            System.out.println(teamName + " does not already contain " + player.getName() + " as a member!");
            return;
        }

        teamMembers.remove(player.getUniqueId());
    }

    /**
     * Check if a player is in a team.
     * @param player
     * @return boolean
     */
    public boolean isInTeam(Player player)
    {
        if (teamMembers.contains(player.getUniqueId()))
            return true;

        return false;
    }

    /**
     * Get a list of UUIDs for each member in the team.
     * @return List<UUID>
     */
    public List<UUID> getTeamMembers()
    {
        return teamMembers;
    }
}
