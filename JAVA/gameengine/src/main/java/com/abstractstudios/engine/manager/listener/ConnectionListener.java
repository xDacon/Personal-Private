package com.abstractstudios.engine.manager.listener;

import com.abstractstudios.engine.GameEngine;
import com.abstractstudios.engine.game.TeamGame;
import com.abstractstudios.engine.game.team.Team;
import com.abstractstudios.engine.manager.state.GameState;
import com.abstractstudios.engine.utils.UtilChat;
import com.abstractstudios.engine.utils.UtilItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by ThatAbstractWolf on 2016-12-23.
 */
public class ConnectionListener implements Listener
{

    public ConnectionListener(GameEngine gameEngine)
    {
        this.gameEngine = gameEngine;
    }

    private GameEngine gameEngine;

    @EventHandler
    public void onConnect(PlayerJoinEvent event)
    {
        if (Bukkit.getOnlinePlayers().size() >= 1)
        {
            //TODO remove this line and make the selected game start when testing is done.
            gameEngine.getGameManager().startGame(gameEngine.getGameManager().getGameByName("team spleef"));
        }

        if (!gameEngine.getGameManager().getCurrentGame().getMembers().contains(event.getPlayer().getUniqueId()))
            gameEngine.getGameManager().getCurrentGame().addMember(event.getPlayer());

        if (gameEngine.getStateManager().getGameState().equals(GameState.WAITING) || gameEngine.getStateManager().getGameState().equals(GameState.COUNTDOWN))
            event.getPlayer().getInventory().setItem(8, new UtilItemBuilder(Material.COMPASS).setName("&bTeam Selector &7(Right Click)").build());
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event)
    {

        if (gameEngine.getGameManager().getCurrentGame() != null)
        {

            if (Bukkit.getOnlinePlayers().size() < 2 && gameEngine.getStateManager().getGameState().equals(GameState.STARTED))
            {
                gameEngine.getGameManager().stopGame();
                gameEngine.getCountdownManager().stopCountdown();
            }
            else if (Bukkit.getOnlinePlayers().size() < (gameEngine.getGameManager().getCurrentGame().getMaxPlayers() / 2) && !gameEngine.getStateManager().getGameState().equals(GameState.STARTED))
            {
                UtilChat.sendBroadcast("Game", "There are not enough players, please wait for more players!");
                gameEngine.getCountdownManager().stopCountdown();
            }
        }

        if (gameEngine.getGameManager().getCurrentGame() instanceof TeamGame)
        {
            TeamGame teamGame = (TeamGame) gameEngine.getGameManager().getCurrentGame();

            for (Team teams : teamGame.getGameTeams())
                if (teams.getTeamMembers().contains(event.getPlayer().getUniqueId()))
                    teams.getTeamMembers().remove(event.getPlayer().getUniqueId());
        }

        if (gameEngine.getGameManager().getCurrentGame().getMembers().contains(event.getPlayer().getUniqueId()))
            gameEngine.getGameManager().getCurrentGame().removeMember(event.getPlayer());
    }
}