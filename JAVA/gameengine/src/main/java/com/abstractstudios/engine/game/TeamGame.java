package com.abstractstudios.engine.game;

import com.abstractstudios.engine.game.team.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public abstract class TeamGame extends Game
{

    public TeamGame(String gameName, String[] gameDescription, int maxPlayers)
    {
        super(gameName, gameDescription, maxPlayers);

        gameTeams = new ArrayList<>();
    }

    private List<Team> gameTeams;

    /**
     * Add a team to the current gamemode.
     * @param team
     */
    public void addTeam(Team team)
    {
        if (gameTeams.contains(team))
        {
            System.out.println(team.getTeamName() + " is already assigned to this gamemode.");
            return;
        }

        gameTeams.add(team);
    }

    /**
     * Get a list of the teams in the game.
     * @return
     */
    public List<Team> getGameTeams()
    {
        return gameTeams;
    }
}
