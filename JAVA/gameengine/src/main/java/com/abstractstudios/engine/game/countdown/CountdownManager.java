package com.abstractstudios.engine.game.countdown;

import com.abstractstudios.engine.GameEngine;
import com.abstractstudios.engine.game.Game;
import com.abstractstudios.engine.game.TeamGame;
import com.abstractstudios.engine.game.countdown.type.PhaseOneCountdown;
import com.abstractstudios.engine.game.team.Team;
import com.abstractstudios.engine.manager.state.GameState;
import com.abstractstudios.engine.utils.UtilChat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class CountdownManager
{

    public CountdownManager(GameEngine gameEngine)
    {
        this.gameEngine = gameEngine;
        this.countdownTypes = new ArrayList<>();
    }

    private GameEngine gameEngine;
    private BukkitTask runnable;

    private List<Countdown> countdownTypes;

    //Defaulted to 30
    private int countdownValue = 30;

    /**
     * Load all of the countdowns.
     */
    public void loadCountdowns()
    {
        addCountdown(new PhaseOneCountdown());
    }

    /**
     * Start a countdown by instance.
     * @param countdown
     */
    public void startCountdown(Countdown countdown, Game game)
    {
        countdownValue = countdown.getTime();

        runnable = new BukkitRunnable()
        {
            @Override
            public void run()
            {

                if (countdownValue == 0)
                {

                    if (Bukkit.getOnlinePlayers().size() < (game.getMaxPlayers() / 2)) // Example: 12 player game / 2 = 6 players for countdown to start
                    {
                        UtilChat.sendBroadcast("Game", "There are not enough players to start the game!");
                        UtilChat.sendBroadcast("Game", "For the game to begin loading you need: &b" + (game.getMaxPlayers() / 2) + "&7/&b" + game.getMaxPlayers());
                        UtilChat.sendBroadcast("Game", "Current: &b" + Bukkit.getOnlinePlayers().size() + "&7/&b" + game.getMaxPlayers());
                        stopCountdown();

                        game.getMembers().forEach(uuid ->
                        {
                            Player member = Bukkit.getPlayer(uuid);
                            member.teleport(Bukkit.getWorld("world").getSpawnLocation()); //Todo change to a stored
                        });

                        return;
                    }

                    if (countdown.isBeforeGame())
                    {
                        if (game instanceof TeamGame)
                        {
                            TeamGame teamGame = (TeamGame) game;

                            for (UUID uuid : game.getMembers())
                            {
                                Player player = Bukkit.getPlayer(uuid); //For messaging later

                                for (int i = 0; i < teamGame.getGameTeams().size(); i++)
                                {
                                    Team teams = teamGame.getGameTeams().get(i);

                                    if (teams.getTeamMembers().contains(uuid))
                                    {
                                        UtilChat.sendMessage(player, "Team", "You are in the " + teams.getTeamColour().getColourEquivalent() + teams.getTeamName() + " &7team!");
                                        System.out.println("Found! Player is in " + teams.getTeamName());
                                        break;
                                    }

                                    if (!teams.getTeamMembers().contains(uuid))
                                    {
                                        int ranTeam = 0; //No team

                                        System.out.println(Bukkit.getOnlinePlayers().size());
                                        if (Bukkit.getOnlinePlayers().size() == 2) //For a 2 player game or 2 players that need to be split.
                                            if(game.getMembers().get(0).equals(uuid))
                                                ranTeam = 0;
                                            else if (game.getMembers().get(1).equals(uuid))
                                                ranTeam = 1;

                                        Team randomTeam = teamGame.getGameTeams().get(ranTeam);
                                        System.out.println(randomTeam.getTeamName() + " - " + player.getName());
                                        randomTeam.addTeamMember(player);
                                        UtilChat.sendMessage(player, "Team", "You are in the " + randomTeam.getTeamColour().getColourEquivalent() + randomTeam.getTeamName() + " &7team!");
                                        break;
                                    }
                                }
                            }


                            System.out.println(teamGame.getGameName() + " is a team game..");

                            System.out.println("There are " + teamGame.getGameTeams().size() + " teams.");

                            for (Team teams : teamGame.getGameTeams())
                                System.out.println(teams.getTeamName() + " - has " + teams.getTeamMembers().size() + " members..");

                        }
                        else
                        {
                            System.out.println(game.getGameName() + " is a non team game.");
                        }

                        System.out.println("Game Member count: " + game.getMembers().size());
                        countdown.onFinish(game);
                        stopCountdown();
                    }
                }


                for (int index : countdown.getIndexes())
                    if (countdownValue == index)
                        UtilChat.sendBroadcast("Countdown", "&b" + game.getGameName() + " &7will start in &b" + countdownValue + " seconds&7!");

                System.out.println("Countdown Value: " + countdownValue);
                countdownValue -= 1;
            }
        }.runTaskTimerAsynchronously(gameEngine, 0L, 20);
    }

    /**
     * Stop a countdown.
     */
    public void stopCountdown()
    {
        if (runnable != null)
            runnable.cancel();
    }

    /**
     * Add a countdown instance to the loaded list.
     * @param countdown
     */
    public void addCountdown(Countdown countdown)
    {
        if (countdownTypes.contains(countdown))
        {
            System.out.println("Countdown instance already loaded.");
            return;
        }

        countdownTypes.add(countdown);
    }

    /**
     * Get a countdown instance via a GameState.
     * @param state
     * @return
     */
    public Countdown getCountdownByState(GameState state)
    {
        for (Countdown countdownType : countdownTypes)
            if (countdownType.getState().equals(state))
                return countdownType;
        return null;
    }
}
