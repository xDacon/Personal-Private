package com.abstractstudios.engine.manager.state.events;

import com.abstractstudios.engine.manager.state.PlayerState;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class PlayerStateUpdateEvent extends Event
{

    public PlayerStateUpdateEvent(Player player, PlayerState newState, PlayerState previousState)
    {
        this.player = player;
        this.previousState = previousState;
        this.newState = newState;
    }

    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private PlayerState previousState, newState;

    public Player getPlayer()
    {
        return player;
    }

    public PlayerState getPreviousState()
    {
        return previousState;
    }

    public PlayerState getNewState()
    {
        return newState;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
