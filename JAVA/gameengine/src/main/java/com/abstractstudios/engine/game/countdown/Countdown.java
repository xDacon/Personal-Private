package com.abstractstudios.engine.game.countdown;

import com.abstractstudios.engine.game.Game;
import com.abstractstudios.engine.manager.state.GameState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public abstract class Countdown
{

    public Countdown(GameState state, int time)
    {
        this.state = state;
        this.time = time;
    }

    private GameState state;
    private int time;

    //Used to check if after the countdown the game will then begin.
    private boolean beforeGame = false;

    private List<Integer> indexes = new ArrayList<>();

    /**
     * Add an index.
     * @param index
     */
    public void addIndex(int... index)
    {
        for (int i : index)
        {
            if (indexes.contains(i))
            {
                System.out.println("Index already exists!");
                return;
            }

            indexes.add(i);
        }
    }

    /**
     * Get a GameState for a countdown.
     * @return GameState
     */
    public GameState getState()
    {
        return state;
    }

    /**
     * Returns the countdown time.
     * Example: 30 seconds = 30
     * @return
     */
    public int getTime()
    {
        return time;
    }

    /**
     * Returns a set of indexes.
     * Indexes are basically values at which the countdown
     * will send a message!
     * @return List<Integer>
     */
    public List<Integer> getIndexes()
    {
        return indexes;
    }

    /**
     * Set the game to start after this countdown.
     * @param beforeGame
     */
    public void setBeforeGame(boolean beforeGame)
    {
        this.beforeGame = beforeGame;
    }

    /**
     * Checks if this countdown is before the game starts.
     * @return boolean
     */
    public boolean isBeforeGame()
    {
        return beforeGame;
    }

    /**
     * Called when the countdown finishes.
     */
    public void onFinish(Game game) {}
}
