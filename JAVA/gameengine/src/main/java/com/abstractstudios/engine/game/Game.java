package com.abstractstudios.engine.game;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public abstract class Game
{

    public Game(String gameName, String[] gameDescription, int maxPlayers)
    {
        this.gameName = gameName;
        this.gameDescription = gameDescription;
        this.maxPlayers = maxPlayers;

        members = new ArrayList<>();

        /*
         * Game Settings..
         */
        pvp = false;
    }

    private String gameName;
    private String[] gameDescription;
    private int maxPlayers;

    //You should add every player here for future, even if a team game! (Automatically done)..
    private List<UUID> members;

    //Game Settings
    public boolean pvp;

    /**
     * Get the Games name.
     * @return String
     */
    public String getGameName()
    {
        return gameName;
    }

    /**
     * Get the games description.
     * @return String[]
     */
    public String[] getGameDescription()
    {
        return gameDescription;
    }

    /**
     * Get the max players for this game.
     * @return int
     */
    public int getMaxPlayers()
    {
        return maxPlayers;
    }

    /**
     * Add a member to the member list, this should contain every player in the game at the time!
     * @param player
     */
    public void addMember(Player player)
    {
        if (members.contains(player.getUniqueId()))
        {
            System.out.println(player.getName() + " is already in the members list!");
            return;
        }

        members.add(player.getUniqueId());
    }

    /**
     * Remove a member to the member list, this should contain every player in the game at the time!
     * Once a spectator gets removed..
     * @param player
     */
    public void removeMember(Player player)
    {
        if (!members.contains(player.getUniqueId()))
        {
            System.out.println(player.getName() + " is not already in the members list!");
            return;
        }

        members.remove(player.getUniqueId());
    }

    /**
     * List of every player in the game.
     * @return List<UUID>
     */
    public List<UUID> getMembers()
    {
        return members;
    }
}
