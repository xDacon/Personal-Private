package com.abstractstudios.engine.manager;

import com.abstractstudios.engine.GameEngine;
import com.abstractstudios.engine.game.Game;
import com.abstractstudios.engine.game.type.Spleef;
import com.abstractstudios.engine.game.type.TeamSpleef;
import com.abstractstudios.engine.manager.state.GameState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class GameManager
{

    public GameManager(GameEngine gameEngine)
    {
        this.gameEngine = gameEngine;
        this.loadedGames = new ArrayList<>();
    }

    private GameEngine gameEngine;
    private Game currentGame;

    private List<Game> loadedGames;

    /**
     * Called on server startup to load
     * all games into the loadedGames list.
     */
    public void loadGamemodes()
    {
        addGame(new Spleef());
        addGame(new TeamSpleef());
    }

    /**
     * Start a game via its class instance.
     * Accessed via 'getGameByName("placeholder")'
     * @param game
     */
    public void startGame(Game game)
    {
        if (currentGame != null)
        {
            System.out.println("Stop the game first!");
            return;
        }

        currentGame = game;
        gameEngine.getCountdownManager().startCountdown(gameEngine.getCountdownManager().getCountdownByState(GameState.COUNTDOWN), game);
        System.out.println("Started: " + game.getGameName());
    }

    /**
     * Stop a game completely.
     */
    public void stopGame()
    {
        gameEngine.getCountdownManager().stopCountdown();
        currentGame = null;
    }

    /**
     * Add a game to the loaded game list.
     * @param game
     */
    public void addGame(Game game)
    {
        if (loadedGames.contains(game))
        {
            System.out.println(game.getGameName() + " is already loaded!");
            return;
        }

        loadedGames.add(game);
    }

    /**
     * Get the current game.
     * @return Game
     */
    public Game getCurrentGame()
    {
        return currentGame;
    }

    public Game getGameByName(String name)
    {
        for (Game games : loadedGames)
            if (games.getGameName().equalsIgnoreCase(name))
                return games;
        return null;
    }
}
