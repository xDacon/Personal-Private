package com.abstractstudios.engine.game.countdown.type;

import com.abstractstudios.engine.game.Game;
import com.abstractstudios.engine.game.countdown.Countdown;
import com.abstractstudios.engine.manager.state.GameState;
import com.abstractstudios.engine.utils.UtilChat;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class PhaseOneCountdown extends Countdown
{

    public PhaseOneCountdown()
    {
        super(GameState.COUNTDOWN, 30);

        addIndex(25, 20, 15, 3, 2, 1);
        setBeforeGame(true);
    }

    @Override
    public void onFinish(Game game)
    {
        UtilChat.sendBroadcast("Game", "&b" + game.getGameName() + " &7has started!");
        System.out.println("Finished Countdown Phase One..");
    }
}
