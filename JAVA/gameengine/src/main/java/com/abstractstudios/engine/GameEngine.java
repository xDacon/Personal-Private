package com.abstractstudios.engine;

import com.abstractstudios.engine.database.MongoDBRepository;
import com.abstractstudios.engine.game.countdown.CountdownManager;
import com.abstractstudios.engine.manager.GameManager;
import com.abstractstudios.engine.manager.StateManager;
import com.abstractstudios.engine.manager.listener.ConnectionListener;
import com.abstractstudios.engine.manager.listener.InteractionListener;
import com.abstractstudios.engine.manager.state.GameState;
import com.abstractstudios.engine.utils.menu.MenuManager;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class GameEngine extends JavaPlugin implements Listener
{

    private MongoDBRepository mongoDBRepository;

    private GameManager gameManager;
    private StateManager stateManager;
    private CountdownManager countdownManager;

    @Override
    public void onEnable()
    {

        /* Establish a connection between client and database */
        mongoDBRepository = new MongoDBRepository(this, "host", 321, "db", "user", "pass");
        mongoDBRepository.openConnection();

        gameManager = new GameManager(this);
        stateManager = new StateManager(this);
        countdownManager = new CountdownManager(this);

        gameManager.loadGamemodes();
        countdownManager.loadCountdowns();

        stateManager.setGameState(GameState.WAITING);

        Bukkit.getPluginManager().registerEvents(new ConnectionListener(this), this);
        Bukkit.getPluginManager().registerEvents(new InteractionListener(this), this);
        Bukkit.getPluginManager().registerEvents(new MenuManager(this), this);
    }

    @Override
    public void onDisable()
    {
        Bukkit.getOnlinePlayers().forEach(all -> all.kickPlayer("Game finished, server restarting!") /*Save Data for Player (Stats etc TODO)*/);
    }

    /**
     * Get the Game Manager.
     * @return GameManager
     */
    public GameManager getGameManager()
    {
        return gameManager;
    }

    /**
     * Get the State Manager.
     * @return StateManager
     */
    public StateManager getStateManager()
    {
        return stateManager;
    }

    /**
     * Get the Countdown Manager.
     * @return CountdownManager
     */
    public CountdownManager getCountdownManager()
    {
        return countdownManager;
    }
}
