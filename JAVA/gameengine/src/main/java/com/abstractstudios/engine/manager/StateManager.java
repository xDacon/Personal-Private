package com.abstractstudios.engine.manager;

import com.abstractstudios.engine.GameEngine;
import com.abstractstudios.engine.manager.state.GameState;
import com.abstractstudios.engine.manager.state.PlayerState;
import com.abstractstudios.engine.manager.state.events.GameStateUpdateEvent;
import com.abstractstudios.engine.manager.state.events.PlayerStateUpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class StateManager
{

    public StateManager(GameEngine gameEngine)
    {
        this.gameEngine = gameEngine;
        this.currentPlayerState = new HashMap<>();

        this.gameState = GameState.WAITING;
    }

    private GameEngine gameEngine;
    private GameState gameState;

    private Map<UUID, PlayerState> currentPlayerState;

    /**
     * Set the games current state.
     * @param state
     */
    public void setGameState(GameState state)
    {
        Bukkit.getPluginManager().callEvent(new GameStateUpdateEvent(gameState, state));
        gameState = state;
        System.out.println("GameState updated to: " + state);
    }

    /**
     * Set a players current state.
     * @param player
     * @param state
     */
    public void setPlayerState(Player player, PlayerState state)
    {
        Bukkit.getPluginManager().callEvent(new PlayerStateUpdateEvent(player, state, (currentPlayerState.get(player.getUniqueId()) == null ? PlayerState.LOBBY : currentPlayerState.get(player.getUniqueId()))));
        currentPlayerState.put(player.getUniqueId(), state);
        System.out.println("Set " + player.getName() + "'s PlayerState to " + state.name());
    }

    /**
     * Get a players current playerstate, will return null if they do not have one!
     * @param player
     * @return PlayerState
     */
    public PlayerState getPlayerState(Player player)
    {
        return currentPlayerState.get(player.getUniqueId());
    }

    /**
     * Get current game state.
     * @return GameState
     */
    public GameState getGameState()
    {
        return gameState;
    }
}
