package com.abstractstudios.engine.manager.listener;

import com.abstractstudios.engine.GameEngine;
import com.abstractstudios.engine.game.team.gui.TeamGUI;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by ThatAbstractWolf on 2016-12-23.
 */
public class InteractionListener implements Listener
{

    public InteractionListener(GameEngine gameEngine)
    {
        this.gameEngine = gameEngine;
    }

    private GameEngine gameEngine;

    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
        {
            if (event.getItem().getType().equals(Material.COMPASS)) //Will have no other items..
            {
                new TeamGUI(gameEngine, event.getPlayer());
            }
        }
    }
}
