package com.abstractstudios.engine.manager.state;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public enum PlayerState
{
    LOBBY,
    PLAYING,
    SPECTATING;
}
