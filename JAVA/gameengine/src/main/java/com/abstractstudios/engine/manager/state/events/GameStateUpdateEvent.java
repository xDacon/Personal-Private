package com.abstractstudios.engine.manager.state.events;

import com.abstractstudios.engine.manager.state.GameState;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public class GameStateUpdateEvent extends Event
{

    public GameStateUpdateEvent(GameState previousState, GameState newState)
    {
        this.previousState = previousState;
        this.newState = newState;
    }

    private static final HandlerList handlers = new HandlerList();
    private GameState previousState, newState;

    public GameState getPreviousState()
    {
        return previousState;
    }

    public GameState getNewState()
    {
        return newState;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
