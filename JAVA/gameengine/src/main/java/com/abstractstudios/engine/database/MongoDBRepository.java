package com.abstractstudios.engine.database;

import com.abstractstudios.engine.GameEngine;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import org.bukkit.scheduler.BukkitRunnable;

import org.bson.Document;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by ThatAbstractWolf on 2016-12-24.
 */
public class MongoDBRepository
{

    public MongoDBRepository(GameEngine gameEngine, String host, int port, String database, String username, String password)
    {
        this.gameEngine = gameEngine;
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    private MongoClient mongoClient;
    private MongoDatabase mongoDB;
    private GameEngine gameEngine;

    private final String host, database, username, password;
    private final int port;

    public void openConnection()
    {
        MongoCredential mongoCredential = MongoCredential.createCredential(username, database, password.toCharArray());
        mongoClient = new MongoClient(new ServerAddress(host, port), Arrays.asList(mongoCredential));
        mongoDB = mongoClient.getDatabase(database);

        if (mongoDB != null)
            System.out.println("Connection established between client and database!");
        else
            System.out.println("Could not establish a clear connection between client and database!");
    }

    /**
     * Get the host (IP) for connection.
     * @return String
     */
    public String getHost()
    {
        return host;
    }

    /**
     * Get the connection port.
     * @return int
     */
    public int getPort()
    {
        return port;
    }

    /**
     * Get the database.
     * @return String
     */
    public String getDatabase()
    {
        return database;
    }

    /**
     * Get the login username.
     * @return String
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * Get the login password.
     * @return String
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Set a value into the collection (MongoDB)
     * @param cause
     * @param sets
     */
    public void set(BasicDBObject cause, String collection, HashMap<String, Object> sets)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$set", sets));
            }
        }.runTaskAsynchronously(gameEngine);
    }

    /**
     * Increase a value (MongoDB)
     * @param cause
     * @param inc
     */
    public void inc(BasicDBObject cause, String collection, HashMap<String, Integer> inc)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$inc", inc));
            }
        }.runTaskAsynchronously(gameEngine);
    }

    /**
     * Increase a value in a document. (MongoDB)
     * @param cause
     * @param inc
     */
    public void inc(BasicDBObject cause, String collection, Document inc)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$inc", inc));
            }
        }.runTaskAsynchronously(gameEngine);
    }

    /**
     * Unset deletes something from the database. (MongoDB)
     * @param cause
     * @param collection
     * @param unsets
     */
    public void unset(BasicDBObject cause, String collection, HashMap<String, Object> unsets)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$unset", unsets));
            }
        }.runTaskAsynchronously(gameEngine);
    }

    /**
     * Add a value to an array unless present. (MongoDB)
     * @param cause
     * @param collection
     * @param path
     * @param value
     * @param addIfAlreadyExists
     */
    public void addToSet(BasicDBObject cause, String collection, String path, Object value, boolean addIfAlreadyExists)
    {
        if (addIfAlreadyExists)
        {
            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$push", new Document(path, value)));
                }
            }.runTaskAsynchronously(gameEngine);
        } else
        {
            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$addToSet", new Document(path, value)));
                }
            }.runTaskAsynchronously(gameEngine);
        }
    }

    /**
     * Remove a value with specific conditions from an array.
     * @param cause
     * @param collection
     * @param path
     * @param value
     */
    public void removeFromSet(BasicDBObject cause, String collection, String path, Object value)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                mongoDB.getCollection(collection).updateOne(cause, new BasicDBObject("$pull", new Document(path, value)
                {
                }));
            }
        }.runTaskAsynchronously(gameEngine);
    }
}
