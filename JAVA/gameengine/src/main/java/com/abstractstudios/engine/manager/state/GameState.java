package com.abstractstudios.engine.manager.state;

/**
 * Created by ThatAbstractWolf on 2016-12-22.
 */
public enum GameState
{
    WAITING,
    COUNTDOWN,
    STARTED,
    RESTARTING;
}
