package com.abstractstudios.engine.utils;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.Arrays;

/**
 * Created by ThatAbstractWolf on 2016-09-03.
 */
public class UtilItemBuilder
{

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    /**
     * Constructor to set the material for the item builder.
     * @param material
     */
    public UtilItemBuilder(Material material)
    {
        itemStack = new ItemStack(material);
        itemMeta = itemStack.getItemMeta();
    }

    /**
     * Constructor to set the material for the item builder with a data value.
     * @param material
     * @param data
     */
    public UtilItemBuilder(Material material, byte data)
    {
        itemStack = new ItemStack(material, 1, data);
        itemMeta = itemStack.getItemMeta();
    }

    /**
     * Set the displayname with colour code support for the item.
     * @param name
     * @return UtilItemBuilder
     */
    public UtilItemBuilder setName(String name)
    {
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }

    /**
     * Set the lore for the item.
     * @param lore
     * @return UtilItemBuilder
     */
    public UtilItemBuilder setLore(String... lore)
    {
        itemMeta.setLore(Arrays.asList(lore));
        return this;
    }

    /**
     * Set the amount of items to give a user.
     * @param amount
     * @return UtilItemBuilder
     */
    public UtilItemBuilder setAmount(int amount)
    {
        itemStack.setAmount(amount);
        return this;
    }

    /**
     * Set the data separately from the constructor.
     * @param data
     * @return UtilItemBuilder
     */
    public UtilItemBuilder setData(byte data)
    {
        itemStack.setData(new MaterialData(data));
        return this;
    }

    /**
     * Build the item into an itemstack.
     * @return ItemStack
     */
    public ItemStack build()
    {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}