package com.abstractwolf.channels.listeners;

import com.abstractwolf.channels.channel.Channel;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import com.abstractwolf.channels.channel.ChannelManager;

public class ChatListener implements Listener
{

	private ChannelManager handler;

	public ChatListener(ChannelManager handler)
	{
		this.handler = handler;
	}

	@EventHandler
	public void onTalk(AsyncPlayerChatEvent event)
	{

		Player player = event.getPlayer();

		/*/
		 * Check if user who sends message owns a channe;
		 * if they do send it to all players who are in the channel
		 * else
		 * don't
		 */

		Channel channel = handler.getChannel(player);
		
		if (channel != null)
		{
			event.setCancelled(true);
			channel.sendChannelMessage(player, event.getMessage());
		}
	}
}
