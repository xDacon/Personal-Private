package com.abstractwolf.channels.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import com.abstractwolf.channels.channel.Channel;
import com.abstractwolf.channels.channel.ChannelManager;

public class ConnectionListener implements Listener
{
	
	private ChannelManager handler;
	
	public ConnectionListener(ChannelManager handler)
	{
		this.handler = handler;
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent event)
	{
		Player player = event.getPlayer();
		
		Channel channel = handler.getChannel(player);
		
		if (channel != null)
			channel.disbandChannel(player);
	}
}