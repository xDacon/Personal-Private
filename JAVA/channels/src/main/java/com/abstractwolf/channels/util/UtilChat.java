package com.abstractwolf.channels.util;

import org.bukkit.entity.Player;

public class UtilChat
{
	
	public static void sendMessage(Player player, String message)
	{
		player.sendMessage(Colour.translate("&7" + message));
	}

	public static void sendMessage(Player player, String prefix, String message)
	{
		player.sendMessage(Colour.translate("&4&l" + prefix + "&8 >> &7" + message));
	}
}
