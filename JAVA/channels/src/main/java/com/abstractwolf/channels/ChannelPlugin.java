package com.abstractwolf.channels;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import com.abstractwolf.channels.channel.ChannelManager;
import com.abstractwolf.channels.command.ChannelCommand;
import com.abstractwolf.channels.listeners.ChatListener;
import com.abstractwolf.channels.listeners.ConnectionListener;
import com.abstractwolf.channels.util.UtilServer;

public class ChannelPlugin extends JavaPlugin
{
	private static ChannelPlugin plugin;
	private ChannelManager handler;

	@Override
	public void onEnable()
	{
		handler = new ChannelManager(this);
		
		Bukkit.getPluginManager().registerEvents(new ChatListener(handler), this);
		Bukkit.getPluginManager().registerEvents(new ConnectionListener(handler), this);
		
		getCommand("channel").setExecutor(new ChannelCommand(this, handler));
		
		UtilServer.print("Finished onEnable()!");
	}

	@Override
	public void onDisable()
	{
		handler.getChannels().clear();
		UtilServer.print("Cleared all channel data!");
	}

	public static ChannelPlugin getPlugin()
	{
		return plugin;
	}
}
