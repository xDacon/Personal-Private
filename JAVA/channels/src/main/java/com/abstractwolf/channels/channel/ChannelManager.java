package com.abstractwolf.channels.channel;

import java.util.ArrayList;
import java.util.List;

import com.abstractwolf.channels.util.Colour;
import com.abstractwolf.channels.util.UtilChat;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ChannelManager
{

	private JavaPlugin plugin;
	private List<Channel> channels;

	public ChannelManager(JavaPlugin plugin)
	{
		this.plugin = plugin;

		channels = new ArrayList<>();
	}

	public void createChannel(Player owner, String channelName)
	{

		if (ownsChannel(owner))
		{
			UtilChat.sendMessage(owner, "Channel", "You already own a channel?");
			return;
		}
		
		for (Channel channels : this.channels)
		{
			if (channels.isChannelMember(owner))
			{
				UtilChat.sendMessage(owner, "Channel", "You are a member of another channel!");
				return;
			}
		}
				
		
		channels.add( new Channel(plugin, channelName, owner));
		UtilChat.sendMessage(owner, "Channel", "You made a new channel named &e" + channelName + "&7! Invite players with &e/channel invite <name>&7!");
		
	}

	public boolean ownsChannel(Player player)
	{

		for (Channel channels : this.channels)
			if (channels.isChannelOwner(player))
				return true;

		return false;
	}

	public Channel getChannel(Player player) 
	{
		for (Channel channel : channels)
		{
			if (channel.isChannelMember(player))
				return channel;
		}
		
		return null;
	}

	public void disband(Player owner, Channel channel) 
	{
		channel.disbandChannel(owner); 
		channels.remove(channel);
	}
	
	public StringBuilder getChannelList(Player player)
	{
		StringBuilder builder = new StringBuilder();
		
		if (channels.size() == 0)
		{
			builder.append(Colour.White + "There are no channels!");
			return builder;
		}
		
		for (Channel channel : channels)
		{
			if (channel.isChannelOwner(player))
			{
				builder.append(Colour.Green + channel.getChannelName() + Colour.Gray + " (" + channel.getOwner().getName() + ")").append(Colour.White + ", ");
			}
			else
			{
				builder.append(Colour.Red + channel.getChannelName() + Colour.Gray + " (" + channel.getOwner().getName() + ")").append(Colour.White + ", ");
			}
		}
		
		return builder;
	}

	public List<Channel> getChannels() { return channels; }
}
