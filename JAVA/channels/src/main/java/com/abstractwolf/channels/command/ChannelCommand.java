package com.abstractwolf.channels.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.abstractwolf.channels.ChannelPlugin;
import com.abstractwolf.channels.channel.Channel;
import com.abstractwolf.channels.util.UtilChat;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import com.abstractwolf.channels.channel.ChannelManager;

public class ChannelCommand implements CommandExecutor, TabCompleter
{

	private ChannelPlugin plugin;
	private ChannelManager handler;

	public ChannelCommand(ChannelPlugin plugin, ChannelManager handler)
	{
		this.plugin = plugin;
		this.handler = handler;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{

		if (sender instanceof Player)
		{
			Player player = (Player) sender;

			if (args.length == 2)
			{

				if (args[0].equalsIgnoreCase("create"))
				{
					if (player.hasPermission("abstract.channel.donor"))
					{
						String channelName = args[1].toLowerCase();
						
						if (channelName.length() < 5 || channelName.length() > 25)
						{
							UtilChat.sendMessage(player, "Channel", "Your channel name is either too short or too long!");
							UtilChat.sendMessage(player, "Channel", "Minimum characters: 5 Maximum Characters: 25");
							return false;
						}

						for (Channel channels : handler.getChannels())
						{
							if (channels.getChannelName().equalsIgnoreCase(channelName))
							{
								UtilChat.sendMessage(player, "Channel", "A channel with the name &e" + channelName + " &7already exists!");
								return false;
							}
						}

						handler.createChannel(player, channelName);
					}
					else
					{
						UtilChat.sendMessage(player, "Channel", "You cannot create channels! Minimum rank: &fIron&7!");
						UtilChat.sendMessage(player, "Channel", "Donors can still add you to their channels though!");
					}
				}
				else if (args[0].equalsIgnoreCase("invite")) 
				{

					Player target = Bukkit.getPlayer(args[1]);

					if (target == null)
					{
						UtilChat.sendMessage(player, "Channel", "That player is not online!");
						return false;
					}

					Channel channel = handler.getChannel(player);

					if (channel == null)
					{
						UtilChat.sendMessage(player, "Channel", "You do not own a channel!");
						return false;
					}

					channel.invite(player, target);
				}
				else if (args[0].equalsIgnoreCase("accept"))
				{
					Player owner = Bukkit.getPlayer(args[1]);

					if (owner == null)
					{
						UtilChat.sendMessage(player, "Channel", "That player is not online!");
						return false;
					}

					Channel channel = handler.getChannel(owner);

					if (channel == null)
					{
						UtilChat.sendMessage(player, "Channel", "They do not own a channel so you have not been invited?");
						return false;
					}

					channel.acceptChannelInvite(player);
				}
				else if (args[0].equalsIgnoreCase("remove"))
				{
					Player target = Bukkit.getPlayer(args[1]);

					if (target == null)
					{
						UtilChat.sendMessage(player, "Channel", "That player is not online!");
						return false;
					}

					Channel channel = handler.getChannel(player);

					if (channel == null)
					{
						UtilChat.sendMessage(player, "Channel", "You do not own a channel!");
						return false;
					}

					channel.kickMember(player, target);
				}
			}
			else if (args.length == 1)
			{

				if (args[0].equalsIgnoreCase("disband"))
				{
					
					Channel channel = handler.getChannel(player);

					if (channel == null)
					{
						UtilChat.sendMessage(player, "Channel", "You do not own a channel!");
						return false;
					}

					channel.disbandChannel(player);
				}
				else if (args[0].equalsIgnoreCase("list"))
				{
					UtilChat.sendMessage(player, "Channel", "List");
					UtilChat.sendMessage(player, handler.getChannelList(player).deleteCharAt(handler.getChannelList(player).length() - 2).toString());
				}
				else if (args[0].equalsIgnoreCase("leave"))
				{
					Channel channel = handler.getChannel(player);

					if (channel == null)
					{
						UtilChat.sendMessage(player, "Channel", "You do not own a channel..");
						return false;
					}

					channel.leaveChannel(player);
				}
			}
			else
			{
				UtilChat.sendMessage(player, "Channel", "Help");
				UtilChat.sendMessage(player, "/Channels <click-tab>");
			}
		}

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args)
	{
		List<String> subCommands = new ArrayList<>();

		if (cmd.getName().equalsIgnoreCase("channel") && args.length == 1)
		{
			subCommands.addAll(Arrays.asList("create", "disband", "invite", "accept", "remove", "leave", "list"));
		}
		else if (cmd.getName().equalsIgnoreCase("channel") && args.length == 2)
		{
			Bukkit.getOnlinePlayers().stream().forEach(all -> 
			{
				if (all.equals((Player) sender))
					return;

				subCommands.add(all.getName());
			});
		}

		return subCommands;
	}
}
