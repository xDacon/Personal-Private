package com.abstractwolf.channels.channel;

import java.util.ArrayList;
import java.util.List;

import com.abstractwolf.channels.util.UtilChat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import com.abstractwolf.channels.util.UtilStrings;

public class Channel
{
	
	private JavaPlugin plugin;
	
	private String channelName;
	private Player owner;
	private List<Player> invites, members;

	public Channel(JavaPlugin plugin, String channelName, Player owner)
	{
	
		
		this.plugin = plugin;
		this.channelName = channelName;
		this.owner = owner;
		
		invites = new ArrayList<>();
		members = new ArrayList<>();
		
		members.add(owner);
	}
	
	public void sendChannelMessage(Player player, String message)
	{
		
		Bukkit.getOnlinePlayers().stream().forEach(all -> 
		{
			if (members.contains(all) || all.hasPermission("abstract.channels.staff"))
				UtilChat.sendMessage(all, UtilStrings.capitalizeFirst(channelName), "&e" + player.getName() + " &8>> &7" + message);
		});
	}
	
	public void invite(Player owner, Player player)
	{
		
		if (!isChannelOwner(owner))
		{
			UtilChat.sendMessage(owner, "Channels", "You're not the channel owner!");
			return;
		}
		
		if (owner.equals(player))
		{
			UtilChat.sendMessage(owner, "Channels", "You cannot invite yourself..");
			return;
		}
		
		if (isInvited(player))
		{
			UtilChat.sendMessage(owner, "Channels", "&e" + player.getName() + " &7is already invited!");
			return;
		}
		
		if (members.size() > 16)
		{
			UtilChat.sendMessage(owner, "Channels", "You already have &e16 &7members in your channel!");
			return;
		}
		
		invites.add(player);
		sendChannelMessage(player, "Invited &e" + player.getName() + " &7to the channel!");
		UtilChat.sendMessage(player, "Channel", "&e" + owner.getName() + " &7invited you to their party! /channel accept");
		
		new BukkitRunnable()
		{
			
			@Override
			public void run()
			{
				if (invites.contains(player))
					invites.remove(player);
			
				if (members.contains(player))
					return;
				
				if (owner != null)
					UtilChat.sendMessage(owner, "Channels", "&e" + player.getName() + "&7's Invite ran out!");
				if (player != null)
					UtilChat.sendMessage(player, "Channels", "&e" + player.getName() + "&7's Invite ran out!");
			}
		}.runTaskLater(plugin, 30 * 20);
	}
	
	public void acceptChannelInvite(Player player)
	{
		
		if (invites.contains(player))
		{
			invites.remove(player);
			members.add(player);
			sendChannelMessage(player, "I accepted your invite and joined the channel! &e:D");
		}
		else
		{
			UtilChat.sendMessage(player, "Channel", "You are not invited to a channel!");
		}
	}
	
	public void leaveChannel(Player player)
	{
		
		if (isChannelOwner(player))
		{
			UtilChat.sendMessage(owner, "Channels", "You cannot leave a channel you own.. &e/channel disband&7!");
			return;
		}
		
		sendChannelMessage(player, "left the channel!");
		members.remove(player);
	}
	
	public void kickMember(Player owner, Player player)
	{
		
		if (!isChannelOwner(owner))
		{
			UtilChat.sendMessage(owner, "Channels", "You do not own this channel!");
			return;
		}
		
		if (owner.equals(player))
		{
			UtilChat.sendMessage(owner, "Channels", "You cannot remove yourself!");
			return;
		}
		
		if (!isChannelMember(player))
		{
			UtilChat.sendMessage(owner, "Channels", "&e" + player.getName() + " &7is not a member of this channel!");
			return;
		}
		
		sendChannelMessage(player, "I was kicked from the channel by&f: &e" + owner.getName() + "&7!");
		members.remove(player);
	}
	
	public void disbandChannel(Player owner)
	{
		
		if (!isChannelOwner(owner))
		{
			UtilChat.sendMessage(owner, "Channels", "You cannot disband a channel you do not own!");
			return;
		}
		
		Bukkit.getOnlinePlayers().stream().forEach(all ->
		{
			if (members.contains(all))
				UtilChat.sendMessage(all, "Channel", "The channel you're in has been &edisbanded&7!");
		});
		
		owner = null;
		
		members.clear();
		invites.clear();
	
	}
	
	public Player getOwner()
	{
		return owner;
	}
	
	public List<Player> getMembers()
	{
		return members;
	}
	
	public List<Player> getInvited()
	{
		return invites;
	}
	
	public String getChannelName()
	{
		return channelName;
	}
	
	public boolean isChannelOwner(Player player)
	{
		
		if (owner.equals(player))
			return true;
		else 
			return false;
	}
	
	public boolean isChannelMember(Player player)
	{
		
		if (members.contains(player))
			return true;
		else 
			return false;
	}
	
	public boolean isInvited(Player player)
	{
		if (invites.contains(player))
			return true;
		else 
			return false;
	}
}
