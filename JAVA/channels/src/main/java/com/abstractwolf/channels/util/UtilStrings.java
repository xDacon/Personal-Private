package com.abstractwolf.channels.util;

public class UtilStrings
{

	public static String capitalizeFirst(String input)
	{
		return input.substring(0, 1).toUpperCase() + input.substring(1);
	}
}
