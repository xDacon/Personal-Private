package com.abstractwolf.rankup.utils;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.Arrays;

/**
 * Created by ThatAbstractWolf on 2016-09-03.
 */
public class ItemBuilder
{

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public ItemBuilder(Material material)
    {
        itemStack = new ItemStack(material);
        itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder(Material material, byte data)
    {
        itemStack = new ItemStack(material, 1, data);
        itemMeta = itemStack.getItemMeta();
    }

    /**
     * Set an items displayname.
     * @param name
     * @return ItemBuilder
     */
    public ItemBuilder setName(String name)
    {
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }

    /**
     * Set an items lore.
     * @param lore
     * @return ItemBuilder
     */
    public ItemBuilder setLore(String... lore)
    {
        itemMeta.setLore(Arrays.asList(lore));
        return this;
    }

    /**
     * Set an items amount.
     * @param amount
     * @return ItemBuilder
     */
    public ItemBuilder setAmount(int amount)
    {
        itemStack.setAmount(amount);
        return this;
    }

    /**
     * Set an items data.
     * @param data
     * @return ItemBuilder
     */
    public ItemBuilder setData(byte data)
    {
        itemStack.setData(new MaterialData(data));
        return this;
    }

    /**
     * Parse/Build an item to an itemstack!
     * @return ItemStack
     */
    public ItemStack build()
    {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}