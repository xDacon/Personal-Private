package com.abstractwolf.rankup.ranks;

/**
 * Created by ThatAbstractWolf on 2016-12-10.
 */
public enum PurchaseType
{
    MONEY,
    LEVEL,
    MONSTER;
}
