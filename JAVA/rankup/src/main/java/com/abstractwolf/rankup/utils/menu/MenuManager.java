package com.abstractwolf.rankup.utils.menu;

import com.abstractwolf.rankup.Rankup;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class MenuManager implements Listener
{

	public MenuManager(Rankup instance)
	{
		this.instance = instance;
	}

	private Rankup instance;

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getInventory() == null) 
			return;

		InventoryHolder holder = event.getInventory().getHolder();
		
		if (holder == null) 
			return;

		if (!(holder instanceof Menu))
			return;

		event.setCancelled(true);
		
		if (!(event.getWhoClicked() instanceof Player)) 
			return;

		Player player = (Player) event.getWhoClicked();
		Menu baylorMenu = (Menu) holder;
		ItemStack clicked = event.getCurrentItem();

		ClickType clickType = event.getClick();

		if (clicked == null || clicked.getType() == Material.AIR)
			return;

		if (clickType == null) 
			return;

		for (MenuItem menuItem : baylorMenu.getItems())
		{
			if (!menuItem.getItemStack().equals(clicked))
				continue;

			menuItem.click(player, clickType);
			break;
		}
	}
}