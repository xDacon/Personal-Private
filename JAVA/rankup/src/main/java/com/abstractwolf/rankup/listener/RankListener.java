package com.abstractwolf.rankup.listener;

import com.abstractwolf.rankup.Rankup;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class RankListener implements Listener
{

    public RankListener(Rankup plugin)
    {
        this.plugin = plugin;
    }

    private Rankup plugin;

    @EventHandler
    public void onTalk(AsyncPlayerChatEvent event)
    {

        if (plugin.getPlayerManager().getPlayer(event.getPlayer()) != null)
        {
            event.setFormat(ChatColor.translateAlternateColorCodes('&',
                    plugin.getConfig().getString("format.levelprefix")
                            .replace("{prefix}", plugin.getPlayerManager().getPlayer(event.getPlayer()).getRank().getRankName())
                            .replace("{name}", event.getPlayer().getName())
                            .replace("{message}", event.getMessage())
            ));
        }
    }
}
