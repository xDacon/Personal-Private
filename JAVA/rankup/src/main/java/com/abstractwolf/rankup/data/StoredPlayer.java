package com.abstractwolf.rankup.data;

import com.abstractwolf.rankup.Rankup;
import com.abstractwolf.rankup.ranks.RankupRank;

import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class StoredPlayer
{

    public StoredPlayer(Rankup plugin, UUID uuid)
    {
        this.plugin = plugin;
        this.uuid = uuid;
    }

    private Rankup plugin;
    private UUID uuid;
    private RankupRank rank;
    private boolean accountLoaded = false;

    /**
     * Load all the player data for a specific player.
     */
    public void loadData()
    {

        if (plugin.getConfig().get("players." + uuid.toString() + ".rank") == null)
            rank = plugin.getRankupManager().getRankList().get(0);
        else
            rank = plugin.getRankupManager().getRankByName(plugin.getConfig().getString("players." + uuid.toString() + ".rank"));

        System.out.println("Loaded data for: " + uuid.toString() + ", they have the current rank of: " + rank.getRankName());
        accountLoaded = true;
    }

    /**
     * Save all the player data for a specific player.
     */
    public void saveData()
    {
        plugin.getConfig().set("players." + uuid.toString() + ".rank", rank.getRankName());
        plugin.saveConfig();
        System.out.println("Saving data for: " + uuid.toString() + ", they have the current rank of: " + rank.getRankName());
    }

    /**
     * Get a storedplayers uuid.
     * @return UUID
     */
    public UUID getUuid()
    {
        return uuid;
    }

    /**
     * Get a storedplayers rank.
     * @return RankupRank
     */
    public RankupRank getRank()
    {
        return rank;
    }

    /**
     * Set a stored players rank. Would recommend calling "saveData()" after running this.
     * @param rank
     */
    public void setRank(RankupRank rank)
    {
        this.rank = rank;
    }

    /**
     * Set a users account as loaded for future checking.
     * @param accountLoaded
     */
    public void setAccountLoaded(boolean accountLoaded)
    {
        this.accountLoaded = accountLoaded;
    }

    /**
     * Check if an account is loaded.
     * @return Boolean
     */
    public boolean isAccountLoaded()
    {
        return accountLoaded;
    }
}
