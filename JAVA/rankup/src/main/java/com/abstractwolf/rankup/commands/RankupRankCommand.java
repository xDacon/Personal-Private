package com.abstractwolf.rankup.commands;

import com.abstractwolf.rankup.Rankup;
import com.abstractwolf.rankup.data.StoredPlayer;
import com.abstractwolf.rankup.inventory.RankupUI;
import com.abstractwolf.rankup.ranks.RankupRank;
import com.abstractwolf.rankup.utils.ChatUtil;
import com.abstractwolf.rankup.utils.ConfigurableMessages;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class RankupRankCommand implements CommandExecutor
{

    public RankupRankCommand(Rankup rankup)
    {
        this.rankup = rankup;
    }

    private Rankup rankup;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {

        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            if (args.length == 3)
            {

                if (args[0].equalsIgnoreCase("set"))
                {
                    OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
                    RankupRank rank = (rankup.getRankupManager().getRankByName(args[2]) == null ? rankup.getRankupManager().getRankList().get(0) : rankup.getRankupManager().getRankByName(args[2]));

                    if (target.isOnline())
                    {
                        StoredPlayer storedPlayer = rankup.getPlayerManager().getPlayer(target.getPlayer());
                        storedPlayer.setRank(rank);
                    }
                    else
                    {
                        rankup.getConfig().set("players." + target.getUniqueId().toString() + ".rank", rank.getRankName());
                        rankup.saveConfig();
                    }

                    ChatUtil.sendMessage(player, ConfigurableMessages.prefix, "You've set &b" + target.getName() + "&7 to &b" + rank.getRankName());
                }
            }
            else
            {
                ChatUtil.sendMessage(player, "/rankupadmin set [player] [rank]");

            }
        }

        return false;
    }
}
