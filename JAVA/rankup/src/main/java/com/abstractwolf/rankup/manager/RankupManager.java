package com.abstractwolf.rankup.manager;

import com.abstractwolf.rankup.Rankup;
import com.abstractwolf.rankup.data.StoredPlayer;
import com.abstractwolf.rankup.ranks.PurchaseType;
import com.abstractwolf.rankup.ranks.RankupRank;
import com.abstractwolf.rankup.utils.ChatUtil;
import com.abstractwolf.rankup.utils.ConfigurableMessages;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class RankupManager
{

    public RankupManager(Rankup plugin)
    {
        this.plugin = plugin;
    }

    private Rankup plugin;
    private List<RankupRank> rankList = new ArrayList<>();

    public void loadRanks()
    {

        plugin.getConfig().getConfigurationSection("ranks").getKeys(false).forEach(index -> {

            RankupRank rank = new RankupRank(
                    index,
                    plugin.getConfig().getInt("ranks." + index + ".cost"),
                    PurchaseType.valueOf(plugin.getConfig().getString("ranks." + index + ".type").toUpperCase()));

            plugin.getConfig().getStringList("ranks." + index + ".commands").forEach(command -> {
                rank.addCommand(command);
                System.out.println("Adding command to " + index + ": " + command);
            });

            rankList.add(rank);
        });
    }

    public boolean hasEnoughForRank(Player player, RankupRank rank, boolean check)
    {
        if (rank.getType().equals(PurchaseType.MONEY))
        {
            int currentAmount = (int) plugin.economy.getBalance(player);
            int nextRankCost = rank.getRankupCost();

            if ((currentAmount - nextRankCost) < 0 || currentAmount == 0)
                return false;
            else
            {

                if (!check)
                {
                    EconomyResponse response = plugin.economy.withdrawPlayer(player, plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankupCost());

                    if (response.transactionSuccess())
                    {
                        ChatUtil.sendMessage(player, ConfigurableMessages.prefix,
                                ConfigurableMessages.rankup_message
                                        .replace("{next_rank}", plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankName())
                                        .replace("{next_rank_cost}", String.valueOf(plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankupCost()))
                                        .replace("{next_rank_type}", plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getType().name())
                        );
                        return true;
                    }
                }
            }
        }
        else if (rank.getType().equals(PurchaseType.LEVEL))
        {
            int currentAmount = player.getLevel();
            int nextRankCost = rank.getRankupCost();

            if ((currentAmount - nextRankCost) < 0 || currentAmount == 0)
                return false;
            else
            {
               if (!check)
               {
                   ChatUtil.sendMessage(player, ConfigurableMessages.prefix,
                           ConfigurableMessages.rankup_message
                                   .replace("{next_rank}", plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankName())
                                   .replace("{next_rank_cost}", String.valueOf(plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankupCost()))
                                   .replace("{next_rank_type}", plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getType().name())
                   );
                   player.setLevel(currentAmount - nextRankCost);
                   return true;
               }
            }
        }
        else if (rank.getType().equals(PurchaseType.MONSTER))
        {

            EntityType spawnerType = EntityType.valueOf(plugin.getConfig().getString("ranks." + rank.getRankName() + ".spawnertype").toUpperCase());

            int overallAmount = 0;

            for (int i = 0; i < player.getInventory().getSize(); i++)
                if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(Material.MOB_SPAWNER) && player.getInventory().getItem(i).getItemMeta().getDisplayName().toLowerCase().contains(ChatColor.YELLOW + spawnerType.name().toLowerCase()))
                    overallAmount += player.getInventory().getItem(i).getAmount();

            int amountToTake = rank.getRankupCost();

            if (overallAmount < amountToTake || overallAmount < 0)
            {
                ChatUtil.sendMessage(player, "&a&lIsland&b&lPvP", "You do not have enough to purchase a " + spawnerType.name());
                return false;
            }

            if (!check)
            {
                ChatUtil.sendMessage(player, ConfigurableMessages.prefix, ConfigurableMessages.rankup_message.replace("{next_rank}", plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankName()).replace("{next_rank_cost}", String.valueOf(plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getRankupCost())).replace("{next_rank_type}", plugin.getRankupManager().getRankByName(plugin.getRankupManager().getNextRank(player)).getType().name()));

                for (int i = 0; i < player.getInventory().getSize(); i++)
                {

                    if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType().equals(Material.MOB_SPAWNER) && player.getInventory().getItem(i).getItemMeta().getDisplayName().toLowerCase().contains(ChatColor.YELLOW + spawnerType.name().toLowerCase()))
                    {
                        if (amountToTake < 0)
                            break;

                        ItemStack item = player.getInventory().getItem(i);

                        if (item.getAmount() <= amountToTake)
                        {
                            player.getInventory().removeItem(item);
                            amountToTake = (amountToTake - item.getAmount() < 0 ? 0 : (amountToTake - item.getAmount()));
                        } else
                        {
                            item.setAmount(item.getAmount() - amountToTake);
                            amountToTake = (amountToTake - item.getAmount() < 0 ? 0 : (amountToTake - item.getAmount()));
                            break;
                        }
                    }
                }
            }
        }

        player.updateInventory();
        return true;
    }

    public String getNextRank(Player player)
    {
        int index = 0;

        try
        {

            for (RankupRank ranks : rankList)
            {
                if (plugin.getPlayerManager().getPlayer(player).getRank().getRankName().equals(ranks.getRankName()))
                    break;
                index++;
            }

            return rankList.get(index + 1).getRankName();
        }
        catch (IndexOutOfBoundsException e)
        {
            System.out.println("Highest index possible!");
            return "Highest";
        }
    }

    public RankupRank getRankByName(String name)
    {

        for (RankupRank rank : rankList)
            if (rank.getRankName().equalsIgnoreCase(name))
                return rank;

        return null;
    }

    public List<RankupRank> getRankList()
    {
        return rankList;
    }

}
