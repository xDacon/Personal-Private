package com.abstractwolf.rankup.manager;

import com.abstractwolf.rankup.Rankup;
import com.abstractwolf.rankup.data.StoredPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class PlayerManager
{

    public PlayerManager(Rankup plugin)
    {
        this.plugin = plugin;
    }

    private Rankup plugin;
    private Map<UUID, StoredPlayer> storedPlayer = new HashMap<>();

    /**
     * Add a player to player data.
     * @param uuid
     */
    public void addPlayer(UUID uuid)
    {
        storedPlayer.put(uuid, new StoredPlayer(plugin, uuid));
    }

    /**
     * Remove a player from playerdata.
     * @param uuid
     */
    public void removePlayer(UUID uuid)
    {
        storedPlayer.remove(uuid);
    }

    /**
     * Get a player from playerdata.
     * @param player
     * @return StoredPlayer
     */
    public StoredPlayer getPlayer(Player player)
    {
        return storedPlayer.get(player.getUniqueId());
    }
}
