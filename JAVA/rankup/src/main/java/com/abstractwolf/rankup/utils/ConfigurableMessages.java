package com.abstractwolf.rankup.utils;

import com.abstractwolf.rankup.Rankup;

/**
 * Created by ThatAbstractWolf on 2016-12-10.
 */
public class ConfigurableMessages
{

    public ConfigurableMessages(Rankup instance)
    {
        this.instance = instance;

        prefix = instance.getConfig().getString("format.prefix");
        rankup_message_spawner = instance.getConfig().getString("format.rankup_message_spawner");
        rankup_message_money = instance.getConfig().getString("format.rankup_message_money");
        rankup_message_levels = instance.getConfig().getString("format.rankup_message_level");
        error_message = instance.getConfig().getString("format.error");
        less_cash_message = instance.getConfig().getString("format.less_cash");
    }

    private static Rankup instance;

    public static String prefix;

    public static String rankup_message_spawner, rankup_message_money, rankup_message_levels;
    public static String error_message;
    public static String less_cash_message;

}
