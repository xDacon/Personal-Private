package com.abstractwolf.rankup.utils.menu;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class MenuItem
{
	
	private final ItemStack itemStack;
	private final int index;

	public MenuItem(int index, ItemStack itemStack)
	{
		this.itemStack = itemStack;
		this.index = index;
	}

	public ItemStack getItemStack()
	{
		return itemStack;
	}

	public int getIndex()
	{
		return index;
	}

	public void click(Player player, ClickType clickType) {}
}
