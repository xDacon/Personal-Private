package com.abstractwolf.rankup.listener;

import com.abstractwolf.rankup.Rankup;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class ConnectionListener implements Listener
{

    public ConnectionListener(Rankup plugin)
    {
        this.plugin = plugin;
    }

    private Rankup plugin;

    @EventHandler
    public void onConnect(PlayerLoginEvent event)
    {
        if (plugin.getPlayerManager().getPlayer(event.getPlayer()) == null)
        {
            System.out.println("Adding player started!");
            plugin.getPlayerManager().addPlayer(event.getPlayer().getUniqueId());
            System.out.println("Adding player completed!");
        }
    }

    @EventHandler
    public void onFullyConnected(PlayerJoinEvent event)
    {

        if (plugin.getPlayerManager().getPlayer(event.getPlayer()) != null && !plugin.getPlayerManager().getPlayer(event.getPlayer()).isAccountLoaded())
        {
            plugin.getPlayerManager().getPlayer(event.getPlayer()).loadData();
            System.out.println("Loading player completed!");
        }
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event)
    {
        if (plugin.getPlayerManager().getPlayer(event.getPlayer()) != null && plugin.getPlayerManager().getPlayer(event.getPlayer()).isAccountLoaded())
        {
            plugin.getPlayerManager().getPlayer(event.getPlayer()).saveData();
            plugin.getPlayerManager().removePlayer(event.getPlayer().getUniqueId());
            System.out.println("Saving player completed!");
        }
    }
}
