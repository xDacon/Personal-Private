package com.abstractwolf.rankup.commands;

import com.abstractwolf.rankup.Rankup;
import com.abstractwolf.rankup.inventory.RankupUI;
import com.abstractwolf.rankup.utils.ChatUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class RankupCommand implements CommandExecutor
{

    public RankupCommand(Rankup rankup)
    {
        this.rankup = rankup;
    }

    private Rankup rankup;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {

        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            new RankupUI(rankup, player);
        }

        return false;
    }
}
