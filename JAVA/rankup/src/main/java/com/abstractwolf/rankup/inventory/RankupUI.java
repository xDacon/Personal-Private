package com.abstractwolf.rankup.inventory;

import com.abstractwolf.rankup.Rankup;
import com.abstractwolf.rankup.ranks.RankupRank;
import com.abstractwolf.rankup.utils.ChatUtil;
import com.abstractwolf.rankup.utils.ConfigurableMessages;
import com.abstractwolf.rankup.utils.ItemBuilder;
import com.abstractwolf.rankup.utils.menu.Menu;
import com.abstractwolf.rankup.utils.menu.MenuItem;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class RankupUI extends Menu
{

    public RankupUI(Rankup instance, Player player)
    {
        super(instance, "Rankup - " + player.getName(), 6);

        String status = ChatColor.RED.toString() + ChatColor.BOLD + "Failed - Report to Developer [ThatAbstractWolf]";
        int currentPosition = 0;
        byte data = 0;

        for (int i = 0; i < instance.getRankupManager().getRankList().size(); i++)
        {
            if (instance.getPlayerManager().getPlayer(player).getRank().getRankName().equals(instance.getRankupManager().getRankList().get(i).getRankName()))
                currentPosition = i;
        }

        for (int i = 0; i < instance.getRankupManager().getRankList().size(); i++)
        {

            if (instance.getRankupManager().getRankList().get(i).getRankName().equalsIgnoreCase(instance.getRankupManager().getRankList().get(0).getRankName()))
            {
                data = 0;
                status = ChatColor.WHITE.toString() + ChatColor.BOLD + "FIRST RANK";

                if (instance.getRankupManager().getRankList().get(i).getRankName().equals(instance.getPlayerManager().getPlayer(player).getRank().getRankName()))
                {
                    status = ChatColor.WHITE.toString() + ChatColor.BOLD + "FIRST RANK & CURRENT RANK";
                }
            }
            else if (instance.getRankupManager().getRankList().get(i).getRankName().equals(instance.getPlayerManager().getPlayer(player).getRank().getRankName()))
            {
                data = 5;
                status = ChatColor.GREEN.toString() + ChatColor.BOLD + "CURRENT";
            }
            else if (i > currentPosition)
            {
                data = 14;
                status = ChatColor.DARK_RED.toString() + ChatColor.BOLD + "LOCKED";
            }
            else if (i < currentPosition)
            {
                data = 7;
                status = ChatColor.GRAY.toString() + ChatColor.BOLD + "UNLOCKED";
            }

            final int checkSlot = i;
            final int nextUnlock = currentPosition + 1;

            RankupRank rank = instance.getRankupManager().getRankList().get(i);

            ItemBuilder item = new ItemBuilder(Material.STAINED_GLASS_PANE, data);
            addItem(new MenuItem(i, item
                    .setName(rank.getRankName())
                    .setLore(status, ChatColor.GRAY + "Cost: " + rank.getRankupCost(), ChatColor.GRAY + "Type: " + rank.getType().name())
                    .build())
            {
                @Override
                public void click(Player player, ClickType clickType)
                {
                    if (nextUnlock == checkSlot)
                    {
                        if (instance.getPlayerManager().getPlayer(player) != null && instance.getPlayerManager().getPlayer(player).isAccountLoaded())
                        {

                            if (instance.getRankupManager().getNextRank(player).equals("Highest"))
                                return;

                            if (instance.getRankupManager().hasEnoughForRank(player, instance.getRankupManager().getRankByName(instance.getRankupManager().getNextRank(player)), true))
                            {
                                instance.getRankupManager().hasEnoughForRank(player, instance.getRankupManager().getRankByName(instance.getRankupManager().getNextRank(player)), false);
                                instance.getPlayerManager().getPlayer(player).setRank(instance.getRankupManager().getRankByName(instance.getRankupManager().getNextRank(player)));
                                instance.getPlayerManager().getPlayer(player).getRank().getCommands().forEach(command -> Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("{player}", player.getName())));
                            }
                            else
                            {
                                ChatUtil.sendMessage(player, ConfigurableMessages.prefix,
                                        ConfigurableMessages.less_cash_message
                                                .replace("{next_rank}", instance.getRankupManager().getRankByName(instance.getRankupManager().getNextRank(player)).getRankName())
                                                .replace("{next_rank_cost}", String.valueOf(instance.getRankupManager().getRankByName(instance.getRankupManager().getNextRank(player)).getRankupCost()))
                                                .replace("{next_rank_type}", instance.getRankupManager().getRankByName(instance.getRankupManager().getNextRank(player)).getType().name())
                                );
                            }
                        }
                        else
                        {
                            ChatUtil.sendMessage(player, ConfigurableMessages.prefix, "Your &4player-data &7has not loaded correctly! To continue restart your client or logout and back in.");
                        }

                        player.closeInventory();
                    }
                }
            });
        }

        openInventory(player);
    }
}
