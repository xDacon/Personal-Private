package com.abstractwolf.rankup.utils;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class SkullBuilder
{

    private ItemStack itemStack;
    private SkullMeta itemMeta;

    public SkullBuilder()
    {
        itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        itemMeta = (SkullMeta) itemStack.getItemMeta();
    }

    /**
     * Set the skulls displayname.
     * @param name
     * @return SkullBuilder
     */
    public SkullBuilder setName(String name)
    {
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }

    /**
     * Set the skulls owner by username.
     * @param name
     * @return SkullBuilder
     */
    public SkullBuilder setOwner(String name)
    {
        itemMeta.setOwner(name);
        return this;
    }

    /**
     * Set an items lore.
     * @param lore
     * @return SkullBuilder
     */
    public SkullBuilder setLore(String... lore)
    {
        itemMeta.setLore(Arrays.asList(lore));
        return this;
    }

    /**
     * Set an items amount.
     * @param amount
     * @return SkullBuilder
     */
    public SkullBuilder setAmount(int amount)
    {
        itemStack.setAmount(amount);
        return this;
    }

    /**
     * Build/Parse a skull into an itemstack.
     * @return
     */
    public ItemStack build()
    {
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
