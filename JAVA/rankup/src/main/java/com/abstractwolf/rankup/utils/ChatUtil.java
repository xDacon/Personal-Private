package com.abstractwolf.rankup.utils;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by ThatAbstractWolf on 2016-11-27.
 */
public class ChatUtil
{

    /**
     * Send a message to a player with colour codes.
     * @param player
     * @param message
     */
    public static void sendMessage(Player player, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7" + message));
    }

    /**
     * Send a message to a player with colour codes and a prefix.
     * @param player
     * @param prefix
     * @param message
     */
    public static void sendMessage(Player player, Object prefix, Object message)
    {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &8>> &7" + message));
    }
}
