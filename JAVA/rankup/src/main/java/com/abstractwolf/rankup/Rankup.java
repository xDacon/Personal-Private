package com.abstractwolf.rankup;

import com.abstractwolf.rankup.commands.RankupCommand;
import com.abstractwolf.rankup.commands.RankupRankCommand;
import com.abstractwolf.rankup.listener.ConnectionListener;
import com.abstractwolf.rankup.listener.RankListener;
import com.abstractwolf.rankup.manager.PlayerManager;
import com.abstractwolf.rankup.manager.RankupManager;
import com.abstractwolf.rankup.utils.ConfigurableMessages;
import com.abstractwolf.rankup.utils.menu.MenuManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class Rankup extends JavaPlugin
{

    //Vault Economy
    public Economy economy = null;

    private PlayerManager playerManager;
    private RankupManager rankupManager;

    @Override
    public void onEnable()
    {

        if (getServer().getPluginManager().getPlugin("Vault") == null)
        {
            System.out.println("No vault installed!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        else
        {
            System.out.println("Vault is on this server! Loading Rankup!");
        }

        setupEconomy();

        saveDefaultConfig();
        saveConfig();

        playerManager = new PlayerManager(this);
        rankupManager = new RankupManager(this);

        Bukkit.getPluginManager().registerEvents(new RankListener(this), this);
        Bukkit.getPluginManager().registerEvents(new MenuManager(this), this);
        Bukkit.getPluginManager().registerEvents(new ConnectionListener(this), this);

        getCommand("rankup").setExecutor(new RankupCommand(this));
        getCommand("rankupadmin").setExecutor(new RankupRankCommand(this));

        rankupManager.loadRanks();

        new ConfigurableMessages(this);

        //Used for loading player data for /reload online players.
        Bukkit.getOnlinePlayers().forEach(all ->
        {
            getPlayerManager().addPlayer(all.getUniqueId());
            getPlayerManager().getPlayer(all).loadData();
        });
    }

    @Override
    public void onDisable()
    {
        Bukkit.getOnlinePlayers().forEach(all ->
        {
            getPlayerManager().getPlayer(all).saveData();
            getPlayerManager().removePlayer(all.getUniqueId());
        });
    }

    //Vault Economy Setup
    private boolean setupEconomy()
    {

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);

        if (rsp == null)
            return false;

        economy = rsp.getProvider();
        return economy != null;
    }

    public PlayerManager getPlayerManager()
    {
        return playerManager;
    }

    public RankupManager getRankupManager()
    {
        return rankupManager;
    }

}
