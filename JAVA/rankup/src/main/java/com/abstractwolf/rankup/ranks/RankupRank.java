package com.abstractwolf.rankup.ranks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2016-12-04.
 */
public class RankupRank
{

    public RankupRank(String rankName, int rankupCost, PurchaseType type)
    {
        this.rankName = rankName;
        this.rankupCost = rankupCost;
        this.type = type;

        this.commands = new ArrayList<>();

        System.out.println("Added: " + rankName);
    }

    private String rankName;
    private int rankupCost;
    private PurchaseType type;
    private List<String> commands;

    public String getRankName()
    {
        return rankName;
    }

    public int getRankupCost()
    {
        return rankupCost;
    }

    public PurchaseType getType()
    {
        return type;
    }

    public void addCommand(String command)
    {
        commands.add(command);
    }

    public List<String> getCommands()
    {
        return commands;
    }
}
