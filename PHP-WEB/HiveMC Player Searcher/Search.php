<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">

<title>HiveMC Player Searcher</title>
<link rel="stylesheet" type="text/css" href="css/css.css">
</head>

<body>
    <div id="topbar">
        <p id="title">HIVEMC Player Searcher</p>
        <img id="hivemc_logo" src="images/logo.png"/>
    </div>
	<div id="search">
		<form action="Search.php" enctype="multipart/form-data" method="POST">
			<input type="text" placeholder="IN GAME NAME" name="username" autocomplete="off">
			<br/>
			<br/>
			<input class="searchSubmit" type="submit" value="Search" name="Search">
		</form>
	</div>

    <footer class="footer">
        <p class="footer-name">Copyright © TheHive - Created by <a href="http://test.com/">ThatAbstractWolf</a></p>
    </footer>
</body>
</html>

<?php

$playerCountURL = "http://api.hivemc.com/v1/server/playercount";

$playerCount = file_get_contents($playerCountURL);

$playerCountData = json_decode($playerCount, true);
echo "<br/><br/><br/><div class=echo_player_count><font color=#d8d8d8><b>" . $playerCountData['count'] . "</font></b> Online Players</div>";

if (isset($_POST['username']))
{
	$username = $_POST ['username'];
	
	error_reporting(E_ERROR | E_PARSE);
	
	getData($username);
}

function getData($username)
{
	$url = "http://api.hivemc.com/v1/player/" . $username;

    $content = file_get_contents($url);

	if (strlen($username) < 3 or strlen($username) > 16)
	{
		echo "<br/><br/><br/><br/><br/<br/><br/><br/><br/><br/><br/><div class=echo_text_not_exists><font color=#373737>Please enter a valid String length!</div>";
		echo "<br/><br/><br/><br/><br/><div class=echo_text_not_exists><font color=#373737>NOTE: Minecraft usernames have a maximum length of 16!</div>";
		return;
	}

	if (!$content)
	{
		echo "<br/><br/><br/><div class=echo_text_not_exists><font color=#d8d8d8><b>" . $username . "</font></b> does not exist in the Hive databases!</div>";
		return;
	}

	$data = json_decode($content, true);

    $uuid = wordwrap($data['UUID'] , 4 , '-' , true );

	echo "<br/><br/><br/><br/>";
	echo "<div class=echo_profile_image><img src=https://minotar.net/helm/$username/100.png alt=ERROR height=50 width=50></div>";
	echo "<br/><div class=echo_text_title>USERNAME/UUID: <br/><div class=echo_text_stats>" . $data['username'] . " / " . $uuid . "</div></div><br/>";
	echo "<br/><div class=echo_text_title>RANK: <br/><div class=echo_text_stats>" . $data['rankName'] . "</div></div><br/>";
	echo "<br/><div class=echo_text_title>TOKENS: <br/><div class=echo_text_stats>" . $data['tokens'] . "</div></div><br/>";
	echo "<br/><div class=echo_text_title>CREDITS: <br/><div class=echo_text_stats>" . $data['credits'] . "</div></div><br/>";
    echo "<br/><div class=echo_text_title>FIRST JOINED: <br/><div class=echo_text_stats>" . gmdate("jS F Y  H:i A", $data['firstLogin']) . "</div></div><br/>";
    echo "<br/><div class=echo_text_title>LAST ONLINE: <br/><div class=echo_text_stats>" . gmdate("jS F Y  H:i A", $data['lastLogin']) . "</div></div>";

}

?>